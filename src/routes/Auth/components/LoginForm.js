import React from "react";
import PropTypes from "prop-types";
import { Field, reduxForm } from "redux-form";
import { TextInput } from "components/FormControl";

import { validateRequired } from "helpers/validator";

const LoginForm = ({ form, handleSubmit, submitting }) => (
  <form className="login-form" onSubmit={handleSubmit}>
    <Field
      component={TextInput}
      type="email"
      id={`${form}-email`}
      name="email"
      label="Email"
    />
    <Field
      component={TextInput}
      type="password"
      id={`${form}-password`}
      name="password"
      label="Password"
    />
    <div className="auth-footer text-right">
      <button type="submit" className="btn btn-primary" disabled={submitting}>
        Đăng nhập
      </button>
    </div>
  </form>
);

LoginForm.propTypes = {
  form: PropTypes.string.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
};

export default reduxForm({
  validate: values => ({
    email: validateRequired(values.email),
    password: validateRequired(values.password),
  }),
})(LoginForm);
