import { connect } from "react-redux";
import { toast } from "react-toastify";

import { history } from "store";
import * as authActions from "store/auth";

import AuthPage from "./AuthPage";

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  login: async values => {
    const result = await dispatch(authActions.login(values));
    if (result && result.user) {
      history.push("/admin");

      const user = result.user;
      toast.success(`Hello, ${user.full_name}`);
    }
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthPage);
