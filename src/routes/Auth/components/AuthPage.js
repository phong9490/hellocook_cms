import React from "react";
import PropTypes from "prop-types";

import LoginForm from "./LoginForm";

class AuthPage extends React.Component {
  render() {
    const { login, className } = this.props;

    return (
      <div className={className}>
        <p className="login-box-msg">Đăng nhập để sử dụng</p>
        <LoginForm form="login-form" onSubmit={login} />
      </div>
    );
  }
}

AuthPage.propTypes = {
  className: PropTypes.string,
  login: PropTypes.func.isRequired,
};

export default AuthPage;
