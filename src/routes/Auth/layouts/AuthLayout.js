import React from "react";
import PropTypes from "prop-types";

class AuthLayout extends React.Component {
  componentDidMount() {
    this.props.updateCoreLayoutClass("login-page");
  }

  render() {
    return (
      <div className="login-box">
        <div className="login-logo">
          <b>HOME FRESH</b> CMS
        </div>
        <div className="login-box-body">{this.props.children}</div>
      </div>
    );
  }
}

AuthLayout.propTypes = {
  updateCoreLayoutClass: PropTypes.func.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default AuthLayout;
