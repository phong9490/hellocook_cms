import { connect } from "react-redux";

import * as themeActions from "store/theme";

import AuthLayout from "./AuthLayout";

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  updateCoreLayoutClass: className =>
    dispatch(themeActions.updateCoreLayoutClass(className)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AuthLayout);
