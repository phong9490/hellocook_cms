import React from "react";
import PropTypes from "prop-types";
import { Route, Switch } from "react-router";

import AuthPage from "./components/AuthPageContainer";
import AuthLayout from "./layouts/AuthLayoutContainer";

const AuthRoute = ({ match: { url } }) => (
  <AuthLayout>
    <Switch>
      <Route path={`${url}/`} exact component={AuthPage} />
    </Switch>
  </AuthLayout>
);

AuthRoute.propTypes = {
  match: PropTypes.object.isRequired,
};

export default AuthRoute;
