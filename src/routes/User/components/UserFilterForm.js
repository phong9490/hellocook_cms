import React from "react";
import PropTypes from "prop-types";
import { map } from "lodash";
import { Field, reduxForm } from "redux-form";
import { TextInput, Select } from "components/FormControl";
import { USER_STATUSES, ROLES } from "helpers/constants";

const statusOptions = map(USER_STATUSES, (label, value) => ({ value, label }));
const roleOptions = map(ROLES, (label, value) => ({ value, label }));

const UserFilterForm = ({ handleSubmit, submitting }) => (
  <form className="inline-form" onSubmit={handleSubmit}>
    <Field
      component={TextInput}
      type="text"
      id="keyword"
      name="keyword"
      label="Keyword"
      placeholder="Enter keyword"
    />
    <Field
      component={Select}
      id="status"
      options={statusOptions}
      name="status"
      label="Status"
      placeholder="Chọn trạng thái"
    />
    <Field
      component={Select}
      id="role"
      options={roleOptions}
      name="role"
      label="Role"
      placeholder="Chọn role"
    />
    <button type="submit" className="btn btn-default" disabled={submitting}>
      <i className="fa fa-search" /> Tìm kiếm
    </button>
  </form>
);

UserFilterForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
};

export default reduxForm({
  validate: values => ({}),
})(UserFilterForm);
