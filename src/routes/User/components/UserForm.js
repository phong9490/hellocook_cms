import React from "react";
import PropTypes from "prop-types";
import { map } from "lodash";
import { Field, reduxForm } from "redux-form";
import { TextInput, Select } from "components/FormControl";
import { validateRequired } from "helpers/validator";
import { USER_STATUSES, ROLES } from "helpers/constants";

const statusOptions = map(USER_STATUSES, (label, value) => ({ value, label }));
const roleOptions = map(ROLES, (label, value) => ({ value, label }));

const UserForm = ({ handleSubmit, submitting, isEdit }) => (
  <form className="modal-form" onSubmit={handleSubmit}>
    <div className="row">
      <div className="col-md-6">
        <Field
          component={TextInput}
          type="text"
          id="last_name"
          name="last_name"
          label="Last name"
          placeholder="Enter last name"
        />
      </div>
      <div className="col-md-6">
        <Field
          component={TextInput}
          type="text"
          id="first_name"
          name="first_name"
          label="First name"
          placeholder="Enter first name"
        />
      </div>
    </div>

    <Field
      component={TextInput}
      type="text"
      id="email"
      name="email"
      label="Email"
      placeholder="Enter email"
    />
    <Field
      component={TextInput}
      type="text"
      id="phone"
      name="phone"
      label="Phone"
      placeholder="Enter phone"
    />
    <Field
      component={Select}
      id="status"
      options={statusOptions}
      name="status"
      label="Status"
      placeholder="Chọn trạng thái"
    />
    {!isEdit && (
      <Field
        component={Select}
        id="role"
        options={roleOptions}
        name="role"
        label="Role"
        placeholder="Chọn role"
      />
    )}

    {!isEdit && (
      <Field
        component={TextInput}
        type="password"
        id="password"
        name="password"
        label="Password"
        placeholder="Enter password"
      />
    )}
    <div className="text-right">
      <button type="submit" className="btn btn-success" disabled={submitting}>
        Save
      </button>
    </div>
  </form>
);

UserForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  isEdit: PropTypes.bool,
};

export default reduxForm({
  validate: (values, ownProps) => ({
    last_name: validateRequired(values.last_name),
    first_name: validateRequired(values.first_name),
    email: validateRequired(values.email),
    phone: validateRequired(values.phone),
    role: validateRequired(values.role),
    status: validateRequired(values.status),
    password: ownProps.isEdit ? false : validateRequired(values.password),
  }),
})(UserForm);
