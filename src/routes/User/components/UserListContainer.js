import { connect } from "react-redux";
import { toast } from "react-toastify";

import * as userActions from "../user";

import UserList from "./UserList";

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  fetch: params => dispatch(userActions.fetch(params)),
  create: async (values, callback) => {
    const result = await dispatch(userActions.create(values));
    if (result && result.user) {
      const user = result.user;
      toast.success(`${user.full_name} được tạo thành công`);

      if (callback) {
        callback();
      }
    }
  },
  update: async (values, callback) => {
    const result = await dispatch(userActions.update(values));
    if (result && result.user) {
      const user = result.user;
      toast.success(`${user.full_name} được sửa thành công`);

      if (callback) {
        callback();
      }
    }
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(UserList);
