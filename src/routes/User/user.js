import http from "helpers/http";

export const fetch = params => async (dispatch, getState) => {
  try {
    const result = await http.get("admin/users", { params });

    return result;
  } catch (err) {
    throw err;
  }
};

export const create = values => async (dispatch, getState) => {
  try {
    const result = await http.post("admin/users", values);

    return result;
  } catch (err) {
    throw err;
  }
};

export const update = values => async (dispatch, getState) => {
  try {
    const result = await http.put(`admin/users/${values.id}`, values);

    return result;
  } catch (err) {
    throw err;
  }
};
