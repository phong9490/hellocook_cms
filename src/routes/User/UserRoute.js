import React from "react";
import PropTypes from "prop-types";
import { Route, Switch } from "react-router";

import { ProtectedRoute } from "components/Admin";
import { ROLE_CODE } from "helpers/constants";

import UserLayout from "./layouts/UserLayout";
import UserList from "./components/UserListContainer";

const UserRoute = ({ match: { url } }) => (
  <UserLayout>
    <Switch>
      <Route
        path={`${url}/`}
        exact
        render={routerProps => (
          <ProtectedRoute {...routerProps} allowedRoles={[ROLE_CODE.ADMIN]}>
            <UserList {...routerProps} />
          </ProtectedRoute>
        )}
      />
    </Switch>
  </UserLayout>
);

UserRoute.propTypes = {
  match: PropTypes.object.isRequired,
};

export default UserRoute;
