import React from "react";
import PropTypes from "prop-types";
import { Route, Switch } from "react-router";

import { ProtectedRoute } from "components/Admin";
import { ROLE_CODE } from "helpers/constants";

import DishLayout from "./layouts/DishLayout";
import DishList from "./components/DishListContainer";

const DishRoute = ({ match: { url } }) => (
  <DishLayout>
    <Switch>
      <Route
        path={`${url}/`}
        exact
        render={routerProps => (
          <ProtectedRoute {...routerProps} allowedRoles={[ROLE_CODE.ADMIN]}>
            <DishList {...routerProps} />
          </ProtectedRoute>
        )}
      />
    </Switch>
  </DishLayout>
);

DishRoute.propTypes = {
  match: PropTypes.object.isRequired,
};

export default DishRoute;
