import React from "react";
import PropTypes from "prop-types";
import { Field, reduxForm } from "redux-form";
import { TextInput } from "components/FormControl";

const DishFilterForm = ({ handleSubmit, submitting }) => (
  <form className="inline-form" onSubmit={handleSubmit}>
    <Field
      component={TextInput}
      type="text"
      id="keyword"
      name="keyword"
      label="Tên món"
      placeholder="Nhập tên món"
    />
    <button type="submit" className="btn btn-default" disabled={submitting}>
      <i className="fa fa-search" /> Tìm kiếm
    </button>
  </form>
);

DishFilterForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
};

export default reduxForm({
  validate: values => ({}),
})(DishFilterForm);
