import React from "react";
import PropTypes from "prop-types";
import ReactTable from "react-table";
import { chain } from "lodash";
import Modal from "react-modal";

import { RBAC } from "components/Admin";
import { ROLE_CODE, DISH_TYPES } from "helpers/constants";
import Pagination from "components/Pagination";

import DishForm from "./DishForm";
import DishFilterForm from "./DishFilterForm";

class DishList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      pages: 1,
      page: 1,
      filters: {},
      sortings: {},
      limit: 10,
      loading: false,
      createFormOpen: false,
      updateFormOpen: false,
    };
    this.handleSearch = this.handleSearch.bind(this);
    this.handleUpdateSort = this.handleUpdateSort.bind(this);
    this.handleUpdatePage = this.handleUpdatePage.bind(this);
    this.handleUpdatePageSize = this.handleUpdatePageSize.bind(this);
    this.handleRefreshData = this.handleRefreshData.bind(this);

    this.handleCreateData = this.handleCreateData.bind(this);
    this.handleUpdateData = this.handleUpdateData.bind(this);

    this.handleOpenCreateForm = this.handleOpenCreateForm.bind(this);
    this.handleCloseCreateForm = this.handleCloseCreateForm.bind(this);

    this.handleOpenUpdateForm = this.handleOpenUpdateForm.bind(this);
    this.handleCloseUpdateForm = this.handleCloseUpdateForm.bind(this);

    this.handleOpenDeleteForm = this.handleOpenDeleteForm.bind(this);
    this.handleCloseDeleteForm = this.handleCloseDeleteForm.bind(this);

    this.columns = [
      {
        Header: "ID",
        accessor: "id",
      },
      {
        Header: "Tên món",
        id: "name",
        accessor: "name",
      },
      {
        Header: "Loại món",
        id: "type",
        accessor: row => DISH_TYPES[row.type],
      },
      {
        Header: "Ảnh",
        accessor: "img",
      },
      {
        id: "action",
        Header: "Action",
        Cell: props => this.renderEdit(props),
        sortable: false,
      },
    ];
  }

  renderEdit({ original }) {
    return (
      <RBAC allowedRoles={[ROLE_CODE.ADMIN]}>
        <div className="btn-group">
          <button
            className="btn btn-warning btn-xs"
            onClick={() => this.handleOpenUpdateForm(original.id)}
          >
            <i className="fa fa-pencil" /> Sửa
          </button>
          <button
            className="btn btn-danger btn-xs"
            onClick={() => this.handleOpenDeleteForm(original.id)}
          >
            <i className="fa fa-trash" /> Xóa
          </button>
          <Modal
            isOpen={this.state.updateFormOpen === original.id}
            onRequestClose={this.handleCloseUpdateForm}
            className="modal-dialog"
            overlayClassName="modal show"
            bodyOpenClassName="modal-open"
            shouldCloseOnOverlayClick
            shouldCloseOnEsc
            shouldFocusAfterRender
            shouldReturnFocusAfterClose
          >
            <div className="modal-content">
              <div className="modal-header">
                <button
                  type="button"
                  className="close"
                  onClick={this.handleCloseUpdateForm}
                >
                  <span>×</span>
                </button>
                <h4 className="modal-title">Sửa Dish</h4>
              </div>
              <div className="modal-body">
                <DishForm
                  form={`dish-update-form-${original.id}`}
                  initialValues={{ ...original, img: [{ url: original.img }] }}
                  onSubmit={this.handleUpdateData}
                  isEdit
                />
              </div>
            </div>
          </Modal>
          <Modal
            isOpen={this.state.deleteFormOpen === original.id}
            onRequestClose={this.handleCloseDeleteForm}
            className="modal-dialog modal-sm"
            overlayClassName="modal show"
            bodyOpenClassName="modal-open"
            shouldCloseOnOverlayClick
            shouldCloseOnEsc
            shouldFocusAfterRender
            shouldReturnFocusAfterClose
          >
            <div className="modal-content">
              <div className="modal-header">
                <button
                  type="button"
                  className="close"
                  onClick={this.handleCloseDeleteForm}
                >
                  <span>×</span>
                </button>
                <h4 className="modal-title">Bạn có chắc muốn xóa món này?</h4>
              </div>
              <div className="modal-body clearfix">
                <button
                  type="button"
                  className="btn btn-primary pull-left"
                  onClick={() => {
                    this.props.deleteDish(original.id, () => {
                      this.handleCloseDeleteForm();
                      this.handleRefreshData();
                    });
                  }}
                >
                  Có
                </button>
                <button
                  type="button"
                  className="btn btn-default pull-right"
                  onClick={this.handleCloseDeleteForm}
                >
                  Không
                </button>
              </div>
            </div>
          </Modal>
        </div>
      </RBAC>
    );
  }

  componentDidMount() {
    this.handleRefreshData();
  }

  async handleRefreshData() {
    const { sortings, filters, page, limit } = this.state;

    this.setState({ loading: true });

    const data = await this.props.fetch({
      sortings,
      filters,
      page,
      limit,
    });

    if (data && data.data) {
      // total page
      const pages = Math.ceil(data.total / data.per_page);
      this.setState({ loading: false, data: data.data, pages });
    } else {
      this.setState({ loading: false });
    }
  }

  handleOpenDeleteForm(id) {
    this.setState({
      deleteFormOpen: id,
    });
  }

  handleCloseDeleteForm() {
    this.setState({
      deleteFormOpen: false,
    });
  }

  handleOpenCreateForm() {
    this.setState({
      createFormOpen: true,
    });
  }

  handleCloseCreateForm() {
    this.setState({
      createFormOpen: false,
    });
  }

  handleCreateData(values) {
    return this.props.create(values, () => {
      this.handleCloseCreateForm();
      this.handleRefreshData();
    });
  }

  handleOpenUpdateForm(id) {
    this.setState({
      updateFormOpen: id,
    });
  }

  handleCloseUpdateForm() {
    this.setState({
      updateFormOpen: false,
    });
  }

  handleUpdateData(values) {
    return this.props.update(values, () => {
      this.handleCloseUpdateForm();
      this.handleRefreshData();
    });
  }

  async handleSearch(values) {
    await this.setState({
      filters: values,
    });
    this.handleRefreshData();
  }

  async handleUpdateSort(values) {
    const sortings = chain(values)
      .keyBy("id")
      .mapValues(row => (row.desc ? "DESC" : "ASC"))
      .value();
    await this.setState({
      sortings,
    });
    this.handleRefreshData();
  }

  async handleUpdatePage(page) {
    await this.setState({
      page: page + 1,
    });
    this.handleRefreshData();
  }

  async handleUpdatePageSize(limit, page) {
    await this.setState({
      page: page + 1,
      limit,
    });
    this.handleRefreshData();
  }

  render() {
    return (
      <div className="box box-primary">
        <div className="box-header with-border">
          <h3 className="box-title">Danh sách món</h3>
        </div>
        <div className="box-body">
          <DishFilterForm
            form="dish-filter-form"
            onSubmit={this.handleSearch}
          />
          <hr />
          <ReactTable
            columns={this.columns}
            data={this.state.data}
            pages={this.state.pages}
            loading={this.state.loading}
            pageSize={this.state.limit}
            page={this.state.page - 1}
            onPageChange={this.handleUpdatePage}
            onPageSizeChange={this.handleUpdatePageSize}
            onSortedChange={this.handleUpdateSort}
            pageSizeOptions={[10, 20, 50, 100]}
            className="-striped -highlight"
            manual
            minRows={0}
            PaginationComponent={Pagination}
          />
        </div>
        <div className="box-footer clearfix no-border text-right">
          <button
            onClick={this.handleOpenCreateForm}
            type="button"
            className="btn btn-default"
          >
            <i className="fa fa-plus" /> Thêm
          </button>
        </div>
        <Modal
          isOpen={this.state.createFormOpen}
          onRequestClose={this.handleCloseCreateForm}
          className="modal-dialog"
          overlayClassName="modal show"
          bodyOpenClassName="modal-open"
          shouldCloseOnOverlayClick
          shouldCloseOnEsc
          shouldFocusAfterRender
          shouldReturnFocusAfterClose
        >
          <div className="modal-content">
            <div className="modal-header">
              <button
                type="button"
                className="close"
                onClick={this.handleCloseCreateForm}
              >
                <span>×</span>
              </button>
              <h4 className="modal-title">Thêm Dish</h4>
            </div>
            <div className="modal-body">
              <DishForm
                form="dish-create-form"
                onSubmit={this.handleCreateData}
              />
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

DishList.propTypes = {
  fetch: PropTypes.func.isRequired,
  create: PropTypes.func.isRequired,
  update: PropTypes.func.isRequired,
  deleteDish: PropTypes.func.isRequired,
};

export default DishList;
