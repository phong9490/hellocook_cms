import React from "react";
import PropTypes from "prop-types";
import { map } from "lodash";
import { Field, reduxForm } from "redux-form";
import { TextInput, Select, FileUpload } from "components/FormControl";
import { validateRequired } from "helpers/validator";
import { DISH_TYPES } from "helpers/constants";

const typeOptions = map(DISH_TYPES, (label, value) => ({ value, label }));

const DishForm = ({ handleSubmit, submitting, isEdit }) => (
  <form className="modal-form" onSubmit={handleSubmit}>
    <Field
      component={TextInput}
      type="text"
      id="name"
      name="name"
      label="Tên món"
    />
    <Field
      component={Select}
      type="number"
      id="type"
      name="type"
      label="Loại món"
      placeholder="Loại món"
      options={typeOptions}
    />
    <Field component={FileUpload} id="img" name="img" />
    <div className="text-right">
      <button type="submit" className="btn btn-success" disabled={submitting}>
        Save
      </button>
    </div>
  </form>
);

DishForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  isEdit: PropTypes.bool,
};

export default reduxForm({
  validate: (values, ownProps) => ({
    name: validateRequired(values.name),
    img: validateRequired(values.img),
    type: validateRequired(values.type),
  }),
})(DishForm);
