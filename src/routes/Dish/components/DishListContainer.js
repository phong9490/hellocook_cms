import { connect } from "react-redux";
import { toast } from "react-toastify";

import * as dishActions from "../dish";

import DishList from "./DishList";

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  fetch: params => dispatch(dishActions.fetch(params)),
  create: async (values, callback) => {
    const result = await dispatch(dishActions.create(values));
    if (result && result.dish) {
      const dish = result.dish;
      toast.success(`${dish.full_name} được tạo thành công`);

      if (callback) {
        callback();
      }
    }
  },

  update: async (values, callback) => {
    const result = await dispatch(dishActions.update(values));
    if (result && result.dish) {
      const dish = result.dish;
      toast.success(`${dish.full_name} được sửa thành công`);

      if (callback) {
        callback();
      }
    }
  },
  deleteDish: async (id, callback) => {
    const result = await dispatch(dishActions.deleteDish(id));
    if (result) {
      toast.success(`Xóa món thành công`);

      if (callback) {
        callback();
      }
    }
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(DishList);
