import http from "helpers/http";

export const fetch = params => async (dispatch, getState) => {
  try {
    const result = await http.get("admin/dishes", { params });

    return result;
  } catch (err) {
    throw err;
  }
};

export const create = values => async (dispatch, getState) => {
  try {
    const result = await http.post("admin/dishes", {
      ...values,
      img: values.img[0].url,
    });

    return result;
  } catch (err) {
    throw err;
  }
};

export const update = values => async (dispatch, getState) => {
  try {
    const result = await http.put(`admin/dishes/${values.id}`, {
      ...values,
      img: values.img[0].url,
    });

    return result;
  } catch (err) {
    throw err;
  }
};

export const deleteDish = id => async (dispatch, getState) => {
  try {
    const result = await http.delete(`admin/dish/${id}`);

    return result;
  } catch (err) {
    throw err;
  }
};
