import React from "react";
import { Route, Switch } from "react-router";
import { Redirect } from "react-router-dom";

import { ProtectedRoute } from "components/Admin";
import { ROLE_CODE } from "helpers/constants";

import AdminRoute from "./Admin/AdminRoute";
import AuthRoute from "./Auth/AuthRoute";

const Routes = () => (
  <Switch>
    <Route path="/auth" component={AuthRoute} />
    <Route
      path="/admin"
      render={routerProps => (
        <ProtectedRoute {...routerProps} allowedRoles={[ROLE_CODE.ADMIN]}>
          <AdminRoute {...routerProps} />
        </ProtectedRoute>
      )}
    />
    <Route path="/" render={() => <Redirect to="/auth" />} />
  </Switch>
);

export default Routes;
