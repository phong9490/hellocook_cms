import React from "react";
import PropTypes from "prop-types";
import { debounce } from "lodash";

import { Header, Footer, Sidebar } from "components/Admin";

const className = "skin-blue sidebar-mini wysihtml5-supported";

class AdminLayout extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      sidebarCollapse: false,
      width: window.innerWidth,
      height: window.innerHeight,
    };

    this.handleToggleCollapse = this.handleToggleCollapse.bind(this);
    this.updateWindowDimensions = debounce(
      this.updateWindowDimensions.bind(this),
      100,
    );
  }

  componentDidMount() {
    this.props.updateCoreLayoutClass(className);
    this.updateWindowDimensions();
    window.addEventListener("resize", this.updateWindowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDimensions);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.sidebarCollapse !== this.state.sidebarCollapse) {
      if (this.state.sidebarCollapse) {
        this.props.updateCoreLayoutClass(className + " sidebar-collapse");
      } else {
        if (this.state.width < 768) {
          this.props.updateCoreLayoutClass(className + " sidebar-open");
        } else {
          this.props.updateCoreLayoutClass(className);
        }
      }
    }
  }

  handleToggleCollapse() {
    this.setState({
      sidebarCollapse: !this.state.sidebarCollapse,
    });
  }

  updateWindowDimensions() {
    this.setState({ width: window.innerWidth, height: window.innerHeight });
  }

  render() {
    return this.props.currentUser ? (
      <div className="wrapper">
        <Header
          toggleCollapse={this.handleToggleCollapse}
          logout={this.props.logout}
          currentUser={this.props.currentUser}
        />
        <Sidebar />
        {this.props.children}
        <Footer />
      </div>
    ) : (
      false
    );
  }
}

AdminLayout.propTypes = {
  currentUser: PropTypes.object,
  logout: PropTypes.func.isRequired,
  updateCoreLayoutClass: PropTypes.func.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default AdminLayout;
