import { connect } from "react-redux";

import { history } from "store";
import * as themeActions from "store/theme";
import * as authActions from "store/auth";
import * as selectors from "store/selectors";

import AdminLayout from "./AdminLayout";

const mapStateToProps = state => ({
  currentUser: selectors.getCurrentUser(state),
});

const mapDispatchToProps = dispatch => ({
  logout: () => {
    dispatch(authActions.logout());
    history.push("/auth");
  },
  updateCoreLayoutClass: className =>
    dispatch(themeActions.updateCoreLayoutClass(className)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AdminLayout);
