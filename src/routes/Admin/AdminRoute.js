import React from "react";
import PropTypes from "prop-types";
import { Route, Switch } from "react-router";

import NotFound from "components/NotFound";

import Dashboard from "./components/Dashboard";
import UserRoute from "../User/UserRoute";
import CustomerRoute from "../Customer/CustomerRoute";
import OrderRoute from "../Order/OrderRoute";
import CardRoute from "../Card/CardRoute";
import DishRoute from "../Dish/DishRoute";
import MenuRoute from "../Menu/MenuRoute";
import AdminLayout from "./layouts/AdminLayoutContainer";

const AdminRoute = ({ match: { url } }) => (
  <AdminLayout>
    <Switch>
      <Route path={`${url}/`} exact component={Dashboard} />
      <Route path={`${url}/user`} component={UserRoute} />
      <Route path={`${url}/customer`} component={CustomerRoute} />
      <Route path={`${url}/card`} component={CardRoute} />
      <Route path={`${url}/dish`} component={DishRoute} />
      <Route path={`${url}/menu`} component={MenuRoute} />
      <Route path={`${url}/order`} component={OrderRoute} />
      <Route
        render={routerProps => (
          <div className="content-wrapper">
            <section className="content">
              <NotFound {...routerProps} />
            </section>
          </div>
        )}
      />
    </Switch>
  </AdminLayout>
);

AdminRoute.propTypes = {
  match: PropTypes.object.isRequired,
};

export default AdminRoute;
