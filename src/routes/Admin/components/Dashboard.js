import React from "react";

const Dashboard = () => (
  <div className="content-wrapper">
    <section className="content-header">
      <h1>
        Admin
        <small>Dashboard</small>
      </h1>
    </section>
  </div>
);

export default Dashboard;
