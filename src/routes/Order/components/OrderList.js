import React from "react";
import PropTypes from "prop-types";
import ReactTable from "react-table";
import { chain } from "lodash";
import Modal from "react-modal";
import moment from "moment";

import OrderDetail from "components/OrderDetailContainer";
import { MENU_TYPES, ORDER_STATUS } from "helpers/constants";
import Pagination from "components/Pagination";

import OrderCreateForm from "./OrderCreateContainer";
import OrderFilterForm from "./OrderFilterForm";

class OrderItemList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      pages: 1,
      page: 1,
      filters: {},
      sortings: {},
      limit: 10,
      loading: false,
      updateFormOpen: false,
      createFormOpen: false,
      deleteFormOpen: false,
    };
    this.handleSearch = this.handleSearch.bind(this);
    this.handleUpdateSort = this.handleUpdateSort.bind(this);
    this.handleUpdatePage = this.handleUpdatePage.bind(this);
    this.handleUpdatePageSize = this.handleUpdatePageSize.bind(this);
    this.handleRefreshData = this.handleRefreshData.bind(this);

    this.handleOpenUpdateForm = this.handleOpenUpdateForm.bind(this);
    this.handleCloseUpdateForm = this.handleCloseUpdateForm.bind(this);
    this.handleUpdate = this.handleUpdate.bind(this);

    this.handleOpenCreateForm = this.handleOpenCreateForm.bind(this);
    this.handleCloseCreateForm = this.handleCloseCreateForm.bind(this);

    this.handleOpenDeleteForm = this.handleOpenDeleteForm.bind(this);
    this.handleCloseDeleteForm = this.handleCloseDeleteForm.bind(this);

    this.columns = [
      {
        Header: "ID",
        id: "id",
        accessor: "uuid",
      },
      {
        Header: "Ngày bán",
        id: "created_at",
        accessor: row => row.menu && moment(row.menu.day).format("DD-MM-YYYY"),
      },
      {
        Header: "Menu",
        sortable: false,
        id: "menu_id",
        accessor: row => row.menu && MENU_TYPES[row.menu.type],
      },
      {
        Header: "Khách hàng",
        sortable: false,
        id: "address",
        accessor: row => {
          return `${row.contact} - ${row.contact_phone}`;
        },
      },
      {
        Header: "Địa chỉ giao hàng",
        sortable: false,
        id: "detail_address",
        accessor: row => {
          return `${row.delivery.address}, ${row.delivery.ward.name}, ${
            row.delivery.district.name
          }, ${row.delivery.province.name}`;
        },
      },
      {
        Header: "Trạng thái",
        id: "status",
        className: "text-center",
        accessor: row => ORDER_STATUS[row.status],
      },
      {
        Header: "Thao tác",
        id: "action",
        Cell: props => this.renderEdit(props),
        sortable: false,
      },
    ];
  }

  renderEdit({ original }) {
    return (
      original.status === 1 && (
        <div className="btn-group">
          <button
            className="btn btn-warning btn-xs"
            onClick={() => this.handleOpenUpdateForm(original.id)}
          >
            <i className="fa fa-pencil" /> Thay đổi
          </button>

          <button
            className="btn btn-danger btn-xs"
            onClick={() => this.handleOpenDeleteForm(original.id)}
          >
            <i className="fa fa-times" /> Huỷ
          </button>
          <Modal
            isOpen={this.state.updateFormOpen === original.id}
            onRequestClose={this.handleCloseUpdateForm}
            className="modal-dialog"
            overlayClassName="modal show"
            bodyOpenClassName="modal-open"
            shouldCloseOnOverlayClick
            shouldCloseOnEsc
            shouldFocusAfterRender
            shouldReturnFocusAfterClose
          >
            <div className="modal-content">
              <div className="modal-header">
                <button
                  type="button"
                  className="close"
                  onClick={this.handleCloseUpdateForm}
                >
                  <span>×</span>
                </button>
                <h4 className="modal-title">Cập nhật đơn hàng</h4>
              </div>
              <div className="modal-body">
                <OrderCreateForm
                  form={`order-update-form-${original.id}`}
                  initialValues={{
                    ...original,
                    country_id: original.delivery.country_id,
                    province_id: original.delivery.province_id,
                    district_id: original.delivery.district_id,
                    ward_id: original.delivery.ward_id,
                    contact: original.delivery.contact,
                    contact_phone: original.delivery.contact_phone,
                    address: original.delivery.address,
                  }}
                  customerId={original.customer_id}
                  day={moment(original.menu.day).format("DD-MM-YYYY")}
                  type={"update"}
                  isEdit
                  callback={() => {
                    this.handleCloseUpdateForm();
                    this.handleRefreshData();
                  }}
                />
              </div>
            </div>
          </Modal>
          <Modal
            isOpen={this.state.deleteFormOpen === original.id}
            onRequestClose={this.handleCloseDeleteForm}
            className="modal-dialog modal-sm"
            overlayClassName="modal show"
            bodyOpenClassName="modal-open"
            shouldCloseOnOverlayClick
            shouldCloseOnEsc
            shouldFocusAfterRender
            shouldReturnFocusAfterClose
          >
            <div className="modal-content">
              <div className="modal-header">
                <button
                  type="button"
                  className="close"
                  onClick={this.handleCloseDeleteForm}
                >
                  <span>×</span>
                </button>
                <h4 className="modal-title">Bạn có chắc muốn huỷ đơn này?</h4>
              </div>
              <div className="modal-body clearfix">
                <button
                  type="button"
                  className="btn btn-primary pull-left"
                  onClick={() => {
                    this.props.cancel(original.id, () => {
                      this.handleCloseDeleteForm();
                      this.handleRefreshData();
                    });
                  }}
                >
                  Có
                </button>
                <button
                  type="button"
                  className="btn btn-default pull-right"
                  onClick={this.handleCloseDeleteForm}
                >
                  Không
                </button>
              </div>
            </div>
          </Modal>
        </div>
      )
    );
  }

  handleOpenCreateForm() {
    this.setState({
      createFormOpen: true,
    });
  }

  handleCloseCreateForm() {
    this.setState({
      createFormOpen: false,
    });
  }

  handleCreateData(values) {
    return this.props.create(values, () => {
      this.handleCloseCreateForm();
      this.handleRefreshData();
    });
  }

  handleOpenUpdateForm(id) {
    this.setState({
      updateFormOpen: id,
    });
  }

  handleCloseUpdateForm() {
    this.setState({
      updateFormOpen: false,
    });
  }

  handleOpenDeleteForm(id) {
    this.setState({
      deleteFormOpen: id,
    });
  }

  handleCloseDeleteForm() {
    this.setState({
      deleteFormOpen: false,
    });
  }

  handleUpdate(values) {
    return this.props.update(values, () => {
      this.handleCloseUpdateForm();
      this.handleRefreshData();
    });
  }

  componentDidMount() {
    this.handleRefreshData();
  }

  async handleRefreshData() {
    const { sortings, filters, page, limit } = this.state;

    this.setState({ loading: true });

    const data = await this.props.fetch({
      sortings,
      filters,
      page,
      limit,
    });

    if (data && data.data) {
      // total page
      const pages = Math.ceil(data.total / data.per_page);
      this.setState({ loading: false, data: data.data, pages });
    } else {
      this.setState({ loading: false });
    }
  }

  async handleSearch(values) {
    await this.setState({
      filters: values,
    });
    this.handleRefreshData();
  }

  async handleUpdateSort(values) {
    const sortings = chain(values)
      .keyBy("id")
      .mapValues(row => (row.desc ? "DESC" : "ASC"))
      .value();
    await this.setState({
      sortings,
    });
    this.handleRefreshData();
  }

  async handleUpdatePage(page) {
    await this.setState({
      page: page + 1,
    });
    this.handleRefreshData();
  }

  async handleUpdatePageSize(limit, page) {
    await this.setState({
      page: page + 1,
      limit,
    });
    this.handleRefreshData();
  }

  render() {
    return (
      <div className="box box-primary">
        <div className="box-header with-border">
          <h3 className="box-title">Danh sách đơn hàng</h3>
        </div>
        <div className="box-body">
          <OrderFilterForm
            form="order-filter-form"
            onSubmit={this.handleSearch}
          />
          <hr />
          <ReactTable
            columns={this.columns}
            data={this.state.data}
            pages={this.state.pages}
            loading={this.state.loading}
            pageSize={this.state.limit}
            page={this.state.page - 1}
            onPageChange={this.handleUpdatePage}
            onPageSizeChange={this.handleUpdatePageSize}
            onSortedChange={this.handleUpdateSort}
            pageSizeOptions={[10, 20, 50, 100]}
            className="-striped -highlight"
            manual
            minRows={0}
            PaginationComponent={Pagination}
            SubComponent={row => <OrderDetail orderId={row.original.id} />}
          />
        </div>
        <div className="box-footer text-right clearfix no-border">
          <button
            onClick={this.handleOpenCreateForm}
            type="button"
            className="btn btn-default"
          >
            <i className="fa fa-plus" /> Thêm đơn
          </button>
        </div>
        <Modal
          isOpen={this.state.createFormOpen}
          onRequestClose={this.handleCloseCreateForm}
          className="modal-dialog"
          overlayClassName="modal show"
          bodyOpenClassName="modal-open"
          shouldCloseOnOverlayClick
          shouldCloseOnEsc
          shouldFocusAfterRender
          shouldReturnFocusAfterClose
        >
          <div className="modal-content">
            <div className="modal-header">
              <button
                type="button"
                className="close"
                onClick={this.handleCloseCreateForm}
              >
                <span>×</span>
              </button>
              <h4 className="modal-title">Thêm order</h4>
            </div>
            <div className="modal-body">
              <OrderCreateForm
                form="order-create-form"
                callback={() => {
                  this.handleCloseCreateForm();
                  this.handleRefreshData();
                }}
              />
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

OrderItemList.propTypes = {
  fetch: PropTypes.func.isRequired,
  create: PropTypes.func.isRequired,
  update: PropTypes.func.isRequired,
  cancel: PropTypes.func.isRequired,
};

export default OrderItemList;
