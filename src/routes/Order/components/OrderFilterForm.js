import React from "react";
import PropTypes from "prop-types";
import { Field, reduxForm } from "redux-form";
import { DateRange, CustomerSelect } from "components/FormControl";

const OrderFilterForm = ({ handleSubmit, submitting }) => (
  <form className="inline-form" onSubmit={handleSubmit}>
    <Field
      component={CustomerSelect}
      id="customer_id"
      name="customer_id"
      label="Khách hàng"
      placeholder="Chọn khách hàng"
    />
    <Field
      component={DateRange}
      startDateId="startDate"
      endDateId="endDate"
      startDatePlaceholder="Ngày bắt đầu"
      endDatePlaceholder="Ngày kết thúc"
      name="date"
      label="Ngày đặt"
      dateFormat="DD-MM-YYYY"
      allowSameDate

    />
    <button type="submit" className="btn btn-default" disabled={submitting}>
      <i className="fa fa-search" /> Tìm kiếm
    </button>
  </form>
);

OrderFilterForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
};

export default reduxForm({
  validate: values => ({}),
})(OrderFilterForm);
