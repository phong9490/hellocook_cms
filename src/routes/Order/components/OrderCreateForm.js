import React from "react";
import PropTypes from "prop-types";
import { Field, reduxForm } from "redux-form";
import { map } from "lodash";
import {
  TextInput,
  CustomerSelect,
  DatePicker,
  Select,
  RegionSelect,
} from "components/FormControl";

import { validateRequired } from "helpers/validator";
import { formatTimeOfDay } from "helpers/helpers";
import { MENU_TYPES, CARD_TYPES } from "helpers/constants";

const timeOptions = [];
for (let i = 360; i <= 1320; i += 30) {
  timeOptions.push({
    value: i,
    label: formatTimeOfDay(i),
  });
}

class OrderCreateForm extends React.Component {
  constructor(props) {
    super(props);
    const { initialValues } = this.props;
    this.state = {
      countryId: 1,
      provinceId: (initialValues && initialValues.province_id) || undefined,
      districtId: (initialValues && initialValues.district_id) || undefined,
      menuOptions: [],
      cardOptions: [],
      addresses: undefined,
    };

    this.handleSelectMenu = this.handleSelectMenu.bind(this);
    this.handleSelectCustomer = this.handleSelectCustomer.bind(this);

    this.handleSelectCountry = this.handleSelectCountry.bind(this);
    this.handleSelectProvince = this.handleSelectProvince.bind(this);
    this.handleSelectDistrict = this.handleSelectDistrict.bind(this);
  }

  componentDidMount() {
    if (this.props.day) {
      this.handleSelectMenu(this.props.day);
    }
    if (this.props.customerId) {
      this.updateCard(this.props.customerId);
    }
  }

  handleSelectCountry(country) {
    this.setState({
      countryId: country ? country.fullData.id : undefined,
    });
  }

  handleSelectProvince(province) {
    this.setState({
      provinceId: province ? province.fullData.id : undefined,
    });
  }

  handleSelectDistrict(district) {
    this.setState({
      districtId: district ? district.fullData.id : undefined,
    });
  }

  async handleSelectMenu(value) {
    const menus = await this.props.searchMenu(value);

    if (menus) {
      const menuOptions = map(menus, menu => ({
        value: menu.id,
        label: `${MENU_TYPES[menu.type]} - ${CARD_TYPES[menu.size]}`,
      }));
      this.setState({
        menuOptions,
      });
    }
  }

  async handleSelectCustomer(value) {
    if (!value) {
      return false;
    }
    this.props.updateInfo(value.fullData.customer);
    this.updateCard(value.fullData.customer.id);

    const addresses = await this.props.getAddress(value.fullData.customer.id);

    if (addresses && addresses.length > 0) {
      const address = addresses[0];
      this.setState({
        provinceId: address.province_id,
        districtId: address.district_id,
      });
      this.props.updateAddress(address);
      // hack
      this.setState({
        provinceId: address.province_id,
        districtId: address.district_id,
      });
    }
  }

  async updateCard(id) {
    const cards = await this.props.searchCardByCustomerId(id);

    if (cards) {
      const cardOptions = map(cards, card => {
        return {
          value: card.id,
          label: CARD_TYPES[card.card_info.type],
        };
      });
      this.setState({
        cardOptions,
      });
    }
  }

  render() {
    const { handleSubmit, submitting } = this.props;

    return (
      <form className="modal-form" onSubmit={handleSubmit}>
        <div className="row">
          <div className="col-md-6">
            <Field
              component={CustomerSelect}
              id="customer_id"
              name="customer_id"
              label="Khách hàng"
              placeholder="Chọn khách hàng"
              callback={value => this.handleSelectCustomer(value)}
            />
          </div>
          <div className="col-md-6">
            <Field
              component={DatePicker}
              id="day"
              name="day"
              label="Ngày"
              placeholder="DD-MM-YYYY"
              dateFormat="DD-MM-YYYY"
              disallowPastDate
              callback={value => this.handleSelectMenu(value)}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Field
              component={TextInput}
              type="text"
              id="diseases"
              name="diseases"
              label="Bệnh"
              readOnly
            />
          </div>
          <div className="col-md-6">
            <Field
              component={TextInput}
              type="text"
              id="back_list"
              name="back_list"
              label="Không ăn được"
              readOnly
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Field
              type="text"
              component={Select}
              id="delivery_from"
              name="delivery_from"
              label="Giờ giao sớm nhất"
              hasLabel
              options={timeOptions}
            />
          </div>
          <div className="col-md-6">
            <Field
              type="text"
              component={Select}
              id="delivery_to"
              name="delivery_to"
              label="Giờ giao muộn nhất"
              hasLabel
              options={timeOptions}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Field
              component={Select}
              options={this.state.menuOptions}
              id="menu_id"
              name="menu_id"
              label="Menu"
              placeholder="Chọn Menu"
            />
          </div>
          <div className="col-md-6">
            <Field
              component={Select}
              options={this.state.cardOptions}
              id="customer_card_id"
              name="customer_card_id"
              label="Card"
              placeholder="Chọn Card"
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Field
              component={TextInput}
              type="text"
              id="contact"
              name="contact"
              label="Người nhận"
              placeholder="Người nhận"
            />
          </div>
          <div className="col-md-6">
            <Field
              component={TextInput}
              type="text"
              id="contact_phone"
              name="contact_phone"
              label="SĐT người nhận"
              placeholder="SĐT người nhận"
            />
          </div>
        </div>
        <Field
          component={TextInput}
          type="text"
          id="address"
          name="address"
          label="Địa chỉ nhận hàng"
          placeholder="Địa chỉ nhận hàng"
        />
        <div className="row">
          <div className="col-md-4">
            <Field
              component={RegionSelect}
              id="province_id"
              name="province_id"
              label="Tỉnh / thành phố"
              placeholder="Tỉnh / thành phố"
              disabled={!this.state.countryId}
              callback={this.handleSelectProvince}
              parentId={this.state.countryId}
            />
          </div>
          <div className="col-md-4">
            <Field
              component={RegionSelect}
              id="district_id"
              name="district_id"
              label="Quận / huyện"
              placeholder="Quận / huyện"
              disabled={!this.state.provinceId}
              callback={this.handleSelectDistrict}
              parentId={this.state.provinceId}
            />
          </div>
          <div className="col-md-4">
            <Field
              component={RegionSelect}
              id="ward_id"
              name="ward_id"
              label="Phường / xã"
              placeholder="Phường / xã"
              disabled={!this.state.districtId}
              parentId={this.state.districtId}
            />
          </div>
        </div>
        <div className="text-right">
          <button
            type="submit"
            className="btn btn-success"
            disabled={submitting}
          >
            Xác nhận
          </button>
        </div>
      </form>
    );
  }
}

OrderCreateForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  searchMenu: PropTypes.func.isRequired,
  updateAddress: PropTypes.func.isRequired,
  updateInfo: PropTypes.func.isRequired,
  day: PropTypes.string,
  getAddress: PropTypes.func.isRequired,
  searchCardByCustomerId: PropTypes.func.isRequired,
  customerId: PropTypes.number,
  initialValues: PropTypes.object,
};

export default reduxForm({
  validate: values => ({
    address: validateRequired(values.address),
    contact: validateRequired(values.contact),
    contact_phone: validateRequired(values.contact_phone),
    customer_id: validateRequired(values.customer_id),
    day: validateRequired(values.day),
    delivery_from: validateRequired(values.delivery_from),
    delivery_to: validateRequired(values.delivery_to),
    district_id: validateRequired(values.district_id),
    province_id: validateRequired(values.province_id),
    ward_id: validateRequired(values.ward_id),
    menu_id: validateRequired(values.menu_id),
    customer_card_id: validateRequired(values.customer_card_id),
  }),
})(OrderCreateForm);
