import { connect } from "react-redux";
import { formValueSelector, change } from "redux-form";
import { toast } from "react-toastify";

import * as orderActions from "../order";
import OrderCreateForm from "./OrderCreateForm";

const mapStateToProps = (state, props) => ({
  selectedProvince: formValueSelector(props.form)(state, "province_id"),
  selectedDisctrict: formValueSelector(props.form)(state, "district_id"),
  selectedWard: formValueSelector(props.form)(state, "ward_id"),
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  onSubmit: async (values, callback) => {
    if (ownProps.type === "update") {
      const result = await dispatch(orderActions.updateOrder(values));
      if (result) {
        toast.success("đơn hàng được cập nhật thành công");
        if (ownProps.callback || callback) {
          ownProps.callback(result);
        }
      }
    } else {
      const result = await dispatch(orderActions.create(values));
      if (result) {
        toast.success("đơn hàng được tạo thành công");
        if (ownProps.callback || callback) {
          ownProps.callback(result);
        }
      }
    }
  },
  searchMenu: day => dispatch(orderActions.fetchMenu(day)),
  searchCardByCustomerId: id =>
    dispatch(orderActions.fetchCardByCustomerId(id)),
  updateInfo: customer => {
    dispatch(change(ownProps.form, "diseases", customer.diseases));
    dispatch(change(ownProps.form, "back_list", customer.back_list));
  },
  updateAddress: address => {
    dispatch(change(ownProps.form, "contact", address.contact));
    dispatch(change(ownProps.form, "contact_phone", address.contact_phone));
    dispatch(change(ownProps.form, "address", address.address));
    dispatch(change(ownProps.form, "province_id", address.province_id));
    dispatch(change(ownProps.form, "district_id", address.district_id));
    dispatch(change(ownProps.form, "ward_id", address.ward_id));
  },
  getAddress: id => dispatch(orderActions.fetchAddress(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderCreateForm);
