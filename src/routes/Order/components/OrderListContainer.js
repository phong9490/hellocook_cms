import { connect } from "react-redux";
import { toast } from "react-toastify";

import * as orderActions from "../order";

import OrderList from "./OrderList";

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  fetch: params => dispatch(orderActions.fetchOrders(params)),
  update: async (values, callback) => {
    const result = await dispatch(orderActions.updateOrder(values));
    if (result) {
      callback();
    }
  },
  create: async (values, callback) => {
    const result = await dispatch(orderActions.create(values));
    if (result && result.order) {
      const order = result.order;
      toast.success(`${order.day} được tạo thành công`);

      if (callback) {
        callback();
      }
    }
  },
  cancel: async (id, callback) => {
    const result = await dispatch(orderActions.cancel(id));
    if (result) {
      toast.success("Huỷ đơn thành công");

      if (callback) {
        callback();
      }
    }
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderList);
