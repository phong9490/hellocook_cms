import { connect } from "react-redux";

import * as orderActions from "../order";

import OrderReview from "./OrderReview";

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  fetch: params => dispatch(orderActions.fetchOrderReviews(params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderReview);
