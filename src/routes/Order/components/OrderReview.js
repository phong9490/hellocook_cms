import React from "react";
import PropTypes from "prop-types";
import ReactTable from "react-table";
import { chain } from "lodash";
import moment from "moment";

import { MENU_TYPES, ORDER_STATUS } from "helpers/constants";
import Pagination from "components/Pagination";

import OrderReviewFilterForm from "./OrderReviewFilterForm";

class OrderReview extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      pages: 1,
      page: 1,
      filters: {},
      sortings: {},
      limit: 10,
      loading: false,
      updateFormOpen: false,
      createFormOpen: false,
    };
    this.handleSearch = this.handleSearch.bind(this);
    this.handleUpdateSort = this.handleUpdateSort.bind(this);
    this.handleUpdatePage = this.handleUpdatePage.bind(this);
    this.handleUpdatePageSize = this.handleUpdatePageSize.bind(this);
    this.handleRefreshData = this.handleRefreshData.bind(this);

    this.columns = [
      {
        Header: "ID",
        id: "id",
        accessor: "uuid",
      },
      {
        Header: "Ngày bán",
        id: "created_at",
        accessor: row => row.menu && moment(row.menu.day).format("DD-MM-YYYY"),
      },
      {
        Header: "Trạng thái",
        id: "status",
        accessor: row => ORDER_STATUS[row.status],
      },
      {
        Header: "Menu",
        sortable: false,
        id: "menu_id",
        accessor: row => row.menu && MENU_TYPES[row.menu.type],
      },
      {
        Header: "Món",
        Cell: props => this.renderDetail(props),
      },
      {
        Header: "Đánh giá",
        Cell: props => this.renderReview(props),
      },
    ];
  }

  renderDetail({ original }) {
    if (!original.detail) {
      return false;
    }

    return (
      <div>
        {chain(original.detail)
          .sortBy(["order_detail_id"], ["ASC"])
          .map((detail, index) => <p key={index}>{detail.name}</p>)
          .value()}
      </div>
    );
  }

  renderReview({ original }) {
    if (!original.detail_review) {
      return false;
    }

    return (
      <div>
        {chain(original.detail_review)
          .sortBy(["order_detail_id"], ["ASC"])
          .map((detail, index) => (
            <p key={index}>
              {detail.rating} * - {detail.comment}
            </p>
          ))
          .value()}
      </div>
    );
  }

  componentDidMount() {
    this.handleRefreshData();
  }

  async handleRefreshData() {
    const { sortings, filters, page, limit } = this.state;

    this.setState({ loading: true });

    const data = await this.props.fetch({
      sortings,
      filters,
      page,
      limit,
    });

    if (data && data.data) {
      // total page
      const pages = Math.ceil(data.total / data.per_page);
      this.setState({ loading: false, data: data.data, pages });
    } else {
      this.setState({ loading: false });
    }
  }

  async handleSearch(values) {
    await this.setState({
      filters: values,
    });
    this.handleRefreshData();
  }

  async handleUpdateSort(values) {
    const sortings = chain(values)
      .keyBy("id")
      .mapValues(row => (row.desc ? "DESC" : "ASC"))
      .value();
    await this.setState({
      sortings,
    });
    this.handleRefreshData();
  }

  async handleUpdatePage(page) {
    await this.setState({
      page: page + 1,
    });
    this.handleRefreshData();
  }

  async handleUpdatePageSize(limit, page) {
    await this.setState({
      page: page + 1,
      limit,
    });
    this.handleRefreshData();
  }

  render() {
    return (
      <div className="box box-primary">
        <div className="box-header with-border">
          <h3 className="box-title">Danh sách đơn hàng</h3>
        </div>
        <div className="box-body">
          <OrderReviewFilterForm
            form="order-filter-form"
            onSubmit={this.handleSearch}
          />
          <hr />
          <ReactTable
            columns={this.columns}
            data={this.state.data}
            pages={this.state.pages}
            loading={this.state.loading}
            pageSize={this.state.limit}
            page={this.state.page - 1}
            onPageChange={this.handleUpdatePage}
            onPageSizeChange={this.handleUpdatePageSize}
            onSortedChange={this.handleUpdateSort}
            pageSizeOptions={[10, 20, 50, 100]}
            className="-striped -highlight"
            manual
            minRows={0}
            PaginationComponent={Pagination}
          />
        </div>
      </div>
    );
  }
}

OrderReview.propTypes = {
  fetch: PropTypes.func.isRequired,
};

export default OrderReview;
