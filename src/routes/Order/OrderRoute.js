import React from "react";
import PropTypes from "prop-types";
import { Route, Switch } from "react-router";

import { ProtectedRoute } from "components/Admin";
import { ROLE_CODE } from "helpers/constants";

import OrderLayout from "./layouts/OrderLayout";
import OrderReview from "./components/OrderReviewContainer";
import OrderList from "./components/OrderListContainer";

const OrderRoute = ({ match: { url } }) => (
  <OrderLayout>
    <Switch>
      <Route
        path={`${url}/`}
        exact
        render={routerProps => (
          <ProtectedRoute {...routerProps} allowedRoles={[ROLE_CODE.ADMIN]}>
            <OrderList {...routerProps} />
          </ProtectedRoute>
        )}
      />
      <Route
        path={`${url}/review`}
        render={routerProps => (
          <ProtectedRoute {...routerProps} allowedRoles={[ROLE_CODE.ADMIN]}>
            <OrderReview {...routerProps} />
          </ProtectedRoute>
        )}
      />
    </Switch>
  </OrderLayout>
);

OrderRoute.propTypes = {
  match: PropTypes.object.isRequired,
};

export default OrderRoute;
