import http from "helpers/http";

export const fetchAddress = id => async (dispatch, getState) => {
  try {
    const result = await http.get(`admin/addresses/${id}`);

    return result;
  } catch (err) {
    throw err;
  }
};

export const fetchOrders = params => async (dispatch, getState) => {
  try {
    const result = await http.get("admin/orders", { params });

    return result;
  } catch (err) {
    throw err;
  }
};

export const fetchOrderReviews = params => async (dispatch, getState) => {
  try {
    const result = await http.get("admin/orders/review", { params });

    return result;
  } catch (err) {
    throw err;
  }
};

export const fetchOrderItems = params => async (dispatch, getState) => {
  try {
    const result = await http.get("admin/order-items", { params });

    return result;
  } catch (err) {
    throw err;
  }
};

export const fetchDeliveries = params => async (dispatch, getState) => {
  try {
    const result = await http.get("admin/deliveries", { params });

    return result;
  } catch (err) {
    throw err;
  }
};

export const create = values => async (dispatch, getState) => {
  try {
    const result = await http.post("admin/orders", values);

    return result;
  } catch (err) {
    throw err;
  }
};

export const fetchMenu = day => async (dispatch, getState) => {
  try {
    const result = await http.get(`admin/menus/${day}`);

    return result;
  } catch (err) {
    throw err;
  }
};

export const fetchCardByCustomerId = id => async (dispatch, getState) => {
  try {
    const result = await http.get(`admin/customercard/${id}`);

    return result;
  } catch (err) {
    throw err;
  }
};

export const fetchOrder = id => async (dispatch, getState) => {
  try {
    const result = await http.get(`admin/orders/${id}`);

    return result;
  } catch (err) {
    throw err;
  }
};

export const updateOrder = values => async (dispatch, getState) => {
  try {
    const result = await http.put(`admin/orders/${values.id}`, values);

    return result;
  } catch (err) {
    throw err;
  }
};

export const cancel = id => async (dispatch, getState) => {
  try {
    const result = await http.put(`admin/orders/cancel/${id}`);

    return result;
  } catch (err) {
    throw err;
  }
};
