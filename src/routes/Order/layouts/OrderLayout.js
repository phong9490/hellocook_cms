import React from "react";
import PropTypes from "prop-types";

class OrderLayout extends React.Component {
  render() {
    return (
      <div className="content-wrapper">
        <section className="content-header">
          <h1>
            Admin
            <small>Order</small>
          </h1>
        </section>
        <section className="content">{this.props.children}</section>
      </div>
    );
  }
}

OrderLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default OrderLayout;
