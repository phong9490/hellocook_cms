import React from "react";
import PropTypes from "prop-types";
import { Route, Switch } from "react-router";

import { ProtectedRoute } from "components/Admin";
import { ROLE_CODE } from "helpers/constants";

import CardLayout from "./layouts/CardLayout";
import CardList from "./components/CardListContainer";

const CardRoute = ({ match: { url } }) => (
  <CardLayout>
    <Switch>
      <Route
        path={`${url}/`}
        exact
        render={routerProps => (
          <ProtectedRoute {...routerProps} allowedRoles={[ROLE_CODE.ADMIN]}>
            <CardList {...routerProps} />
          </ProtectedRoute>
        )}
      />
    </Switch>
  </CardLayout>
);

CardRoute.propTypes = {
  match: PropTypes.object.isRequired,
};

export default CardRoute;
