import React from "react";
import PropTypes from "prop-types";
import { map } from "lodash";
import { Field, reduxForm } from "redux-form";
import { TextInput, Select } from "components/FormControl";
import { validateRequired } from "helpers/validator";
import { USER_STATUSES, CARD_TYPES } from "helpers/constants";

const statusOptions = map(USER_STATUSES, (label, value) => ({ value, label }));
const typeOptions = map(CARD_TYPES, (label, value) => ({ value, label }));

const CardForm = ({ handleSubmit, submitting, isEdit }) => (
  <form className="modal-form" onSubmit={handleSubmit}>
    <Field
      component={TextInput}
      type="text"
      id="code"
      name="code"
      label="Mã card"
      readOnly={isEdit}
    />
    <Field
      component={TextInput}
      type="number"
      id="order_count"
      name="order_count"
      label="Số bữa sử dụng"
      placeholder="Số bữa sử dụng"
    />
    <Field
      component={TextInput}
      type="number"
      id="expire_duration"
      name="expire_duration"
      label="Số ngày sử dụng"
      placeholder="Số ngày sử dụng"
    />
    <Field
      component={Select}
      id="type"
      options={typeOptions}
      name="type"
      label="Loại gói"
      placeholder="Chọn loại gói"
    />
    <Field
      component={Select}
      id="status"
      options={statusOptions}
      name="status"
      label="Status"
      placeholder="Chọn trạng thái"
    />
    <div className="text-right">
      <button type="submit" className="btn btn-success" disabled={submitting}>
        Save
      </button>
    </div>
  </form>
);

CardForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  isEdit: PropTypes.bool,
};

export default reduxForm({
  validate: (values, ownProps) => ({
    code: validateRequired(values.code),
    expire_duration: validateRequired(values.expire_duration),
    order_count: validateRequired(values.order_count),
    status: validateRequired(values.status),
  }),
})(CardForm);
