import { connect } from "react-redux";
import { toast } from "react-toastify";

import * as cardActions from "../card";

import CardList from "./CardList";

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  fetch: params => dispatch(cardActions.fetch(params)),
  create: async (values, callback) => {
    const result = await dispatch(cardActions.create(values));
    if (result && result.card) {
      const card = result.card;
      toast.success(`${card.code} được tạo thành công`);

      if (callback) {
        callback();
      }
    }
  },

  update: async (values, callback) => {
    const result = await dispatch(cardActions.update(values));
    if (result && result.card) {
      const card = result.card;
      toast.success(`${card.code} được sửa thành công`);

      if (callback) {
        callback();
      }
    }
  },
  delete: async (id, callback) => {
    const result = await dispatch(cardActions.deleteCard(id));
    if (result) {
      toast.success("Xóa card thành công");

      if (callback) {
        callback();
      }
    }
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(CardList);
