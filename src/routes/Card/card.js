import http from "helpers/http";

export const fetch = params => async (dispatch, getState) => {
  try {
    const result = await http.get("admin/cards", { params });

    return result;
  } catch (err) {
    throw err;
  }
};

export const create = values => async (dispatch, getState) => {
  try {
    const result = await http.post("admin/cards", values);

    return result;
  } catch (err) {
    throw err;
  }
};

export const update = values => async (dispatch, getState) => {
  try {
    const result = await http.put(`admin/cards/${values.id}`, values);

    return result;
  } catch (err) {
    throw err;
  }
};

export const deleteCard = id => async (dispatch, getState) => {
  try {
    const result = await http.delete(`admin/cards/${id}`);

    return result;
  } catch (err) {
    throw err;
  }
};
