import { connect } from "react-redux";

import * as customerActions from "../customer";

import CustomerAddress from "./CustomerAddress";

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  fetch: id => dispatch(customerActions.fetchAddress(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CustomerAddress);
