import React from "react";
import { map } from "lodash";
import PropTypes from "prop-types";

class CustomerAddress extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      addresses: undefined,
      loading: false,
    };
  }

  async handleFetchData() {
    this.setState({
      loading: true,
    });
    const data = await this.props.fetch(this.props.customerId);

    if (data && data.addresses) {
      this.setState({
        addresses: data.addresses,
        loading: false,
      });
    } else {
      this.setState({
        loading: false,
      });
    }
  }

  componentDidMount() {
    this.handleFetchData();
  }

  render() {
    const { loading, addresses } = this.state;

    if (loading) {
      return (
        <div className="order-item-detail-container text-center">
          <i className="fa fa-circle-o-notch fa-spin loading-icon" />
        </div>
      );
    }

    if (!addresses) {
      return false;
    }

    return (
      <div className="order-item-detail-container">
        <div className="table-responsive">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>STT</th>
                <th>ID</th>
                <th>Tên</th>
                <th>Số điện thoại</th>
                <th>Địa chỉ</th>
              </tr>
            </thead>
            <tbody>
              {map(addresses, (detail, index) => (
                <tr key={index}>
                  <td className="text-center">{index + 1}</td>
                  <td>{detail.id}</td>
                  <td>{detail.contact}</td>
                  <td>{detail.contact_phone}</td>
                  <td>{`${detail.address}, ${
                    detail.ward ? detail.ward.name : ""
                  }, ${detail.district ? detail.district.name : ""}, ${
                    detail.province ? detail.province.name : ""
                  }`}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

CustomerAddress.propTypes = {
  customerId: PropTypes.number.isRequired,
  fetch: PropTypes.func.isRequired,
};

export default CustomerAddress;
