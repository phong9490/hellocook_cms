import React from "react";
import PropTypes from "prop-types";
import { Field, reduxForm } from "redux-form";
import { TextInput } from "components/FormControl";
import { validateRequired } from "helpers/validator";

const CustomerForm = ({ handleSubmit, submitting, isEdit }) => (
  <form className="modal-form" onSubmit={handleSubmit}>
    <div className="row">
      <div className="col-md-6">
        <Field
          component={TextInput}
          type="text"
          id="last_name"
          name="last_name"
          label="Last name"
          placeholder="Enter last name"
        />
      </div>
      <div className="col-md-6">
        <Field
          component={TextInput}
          type="text"
          id="first_name"
          name="first_name"
          label="First name"
          placeholder="Enter first name"
        />
      </div>
    </div>

    <Field
      component={TextInput}
      type="text"
      id="email"
      name="email"
      label="Email"
      placeholder="Enter email"
    />
    <Field
      component={TextInput}
      type="text"
      id="phone"
      name="phone"
      label="Phone"
      placeholder="Enter phone"
    />
    <Field
      component={TextInput}
      type="text"
      id="diseases"
      name="diseases"
      label="Bệnh lý"
      placeholder="Nhập bệnh lý"
    />
    <Field
      component={TextInput}
      type="text"
      id="back_list"
      name="back_list"
      label="Món không ăn"
      placeholder="Món không ăn"
    />
    <div className="text-right">
      <button type="submit" className="btn btn-success" disabled={submitting}>
        Save
      </button>
    </div>
  </form>
);

CustomerForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  isEdit: PropTypes.bool,
};

export default reduxForm({
  validate: (values, ownProps) => ({
    last_name: validateRequired(values.last_name),
    first_name: validateRequired(values.first_name),
    email: validateRequired(values.email),
    phone: validateRequired(values.phone),
  }),
})(CustomerForm);
