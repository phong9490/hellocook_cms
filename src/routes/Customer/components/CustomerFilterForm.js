import React from "react";
import PropTypes from "prop-types";
import { map } from "lodash";
import { Field, reduxForm } from "redux-form";
import { TextInput, Select } from "components/FormControl";
import { USER_STATUSES } from "helpers/constants";

const statusOptions = map(USER_STATUSES, (label, value) => ({
  value,
  label,
}));

const CustomerFilterForm = ({ handleSubmit, submitting }) => (
  <form className="inline-form" onSubmit={handleSubmit}>
    <Field
      component={TextInput}
      type="text"
      id="keyword"
      name="keyword"
      label="Keyword"
      placeholder="Enter keyword"
    />
    <Field
      component={Select}
      id="status"
      options={statusOptions}
      name="status"
      label="Status"
      placeholder="Chọn trạng thái"
    />
    <button type="submit" className="btn btn-default" disabled={submitting}>
      <i className="fa fa-search" /> Tìm kiếm
    </button>
  </form>
);

CustomerFilterForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
};

export default reduxForm({
  validate: values => ({}),
})(CustomerFilterForm);
