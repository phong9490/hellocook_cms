import { connect } from "react-redux";
import { toast } from "react-toastify";

import * as customerActions from "../customer";

import CustomerList from "./CustomerList";

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  fetch: params => dispatch(customerActions.fetch(params)),
  update: async (values, callback) => {
    const result = await dispatch(customerActions.update(values));

    if (result && result.user) {
      const user = result.user;
      toast.success(`${user.full_name} được sửa thành công`);

      if (callback) {
        callback();
      }
    }
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(CustomerList);
