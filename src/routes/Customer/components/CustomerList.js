import React from "react";
import PropTypes from "prop-types";
import ReactTable from "react-table";
import { chain } from "lodash";
import Modal from "react-modal";

import { RBAC } from "components/Admin";
import { ROLE_CODE } from "helpers/constants";
import Pagination from "components/Pagination";
import CustomerAddress from "./CustomerAddressContainer";

import CustomerForm from "./CustomerForm";
import CustomerFilterForm from "./CustomerFilterForm";

class CustomerList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: [],
      pages: 1,
      page: 1,
      filters: {},
      sortings: {},
      limit: 10,
      loading: false,
      createFormOpen: false,
      updateFormOpen: false,
    };
    this.handleSearch = this.handleSearch.bind(this);
    this.handleUpdateSort = this.handleUpdateSort.bind(this);
    this.handleUpdatePage = this.handleUpdatePage.bind(this);
    this.handleUpdatePageSize = this.handleUpdatePageSize.bind(this);
    this.handleRefreshData = this.handleRefreshData.bind(this);

    this.handleUpdateData = this.handleUpdateData.bind(this);

    this.handleOpenCreateForm = this.handleOpenCreateForm.bind(this);
    this.handleCloseCreateForm = this.handleCloseCreateForm.bind(this);

    this.handleOpenUpdateForm = this.handleOpenUpdateForm.bind(this);
    this.handleCloseUpdateForm = this.handleCloseUpdateForm.bind(this);

    this.columns = [
      {
        Header: "ID",
        accessor: "id",
      },
      {
        Header: "Name",
        id: "name",
        accessor: row => row.user && row.user.full_name,
      },
      {
        Header: "Email",
        id: "email",
        accessor: row => row.user && row.user.email,
      },
      {
        Header: "Phone",
        id: "phone",
        accessor: row => row.user && row.user.phone,
      },
      {
        Header: "Món không ăn",
        id: "back_list",
        accessor: "back_list",
      },
      {
        Header: "Bệnh lý",
        id: "diseases",
        accessor: "diseases",
      },
      {
        id: "action",
        Header: "Action",
        Cell: props => this.renderEdit(props),
        sortable: false,
      },
    ];
  }

  renderEdit({ original }) {
    return (
      <RBAC allowedRoles={[ROLE_CODE.ADMIN]}>
        <div className="btn-group">
          <button
            className="btn btn-warning btn-xs"
            onClick={() => this.handleOpenUpdateForm(original.id)}
          >
            <i className="fa fa-pencil" /> Sửa
          </button>
          <Modal
            isOpen={this.state.updateFormOpen === original.id}
            onRequestClose={this.handleCloseUpdateForm}
            className="modal-dialog"
            overlayClassName="modal show"
            bodyOpenClassName="modal-open"
            shouldCloseOnOverlayClick
            shouldCloseOnEsc
            shouldFocusAfterRender
            shouldReturnFocusAfterClose
          >
            <div className="modal-content">
              <div className="modal-header">
                <button
                  type="button"
                  className="close"
                  onClick={this.handleCloseUpdateForm}
                >
                  <span>×</span>
                </button>
                <h4 className="modal-title">Sửa Customer</h4>
              </div>
              <div className="modal-body">
                <CustomerForm
                  form={`customer-update-form-${original.id}`}
                  initialValues={{
                    ...original,
                    first_name: original.user.first_name,
                    last_name: original.user.last_name,
                    phone: original.user.phone,
                    email: original.user.email,
                  }}
                  onSubmit={this.handleUpdateData}
                  isEdit
                />
              </div>
            </div>
          </Modal>
        </div>
      </RBAC>
    );
  }

  componentDidMount() {
    this.handleRefreshData();
  }

  async handleRefreshData() {
    const { sortings, filters, page, limit } = this.state;

    this.setState({ loading: true });

    const data = await this.props.fetch({
      sortings,
      filters,
      page,
      limit,
    });

    if (data && data.data) {
      // total page
      const pages = Math.ceil(data.total / data.per_page);
      this.setState({ loading: false, data: data.data, pages });
    } else {
      this.setState({ loading: false });
    }
  }

  handleOpenCreateForm() {
    this.setState({
      createFormOpen: true,
    });
  }

  handleCloseCreateForm() {
    this.setState({
      createFormOpen: false,
    });
  }

  handleOpenUpdateForm(id) {
    this.setState({
      updateFormOpen: id,
    });
  }

  handleCloseUpdateForm() {
    this.setState({
      updateFormOpen: false,
    });
  }

  handleUpdateData(values) {
    return this.props.update(values, () => {
      this.handleCloseUpdateForm();
      this.handleRefreshData();
    });
  }

  async handleSearch(values) {
    await this.setState({
      filters: values,
    });
    this.handleRefreshData();
  }

  async handleUpdateSort(values) {
    const sortings = chain(values)
      .keyBy("id")
      .mapValues(row => (row.desc ? "DESC" : "ASC"))
      .value();
    await this.setState({
      sortings,
    });
    this.handleRefreshData();
  }

  async handleUpdatePage(page) {
    await this.setState({
      page: page + 1,
    });
    this.handleRefreshData();
  }

  async handleUpdatePageSize(limit, page) {
    await this.setState({
      page: page + 1,
      limit,
    });
    this.handleRefreshData();
  }

  render() {
    return (
      <div className="box box-primary">
        <div className="box-header with-border">
          <h3 className="box-title">Danh sách customer</h3>
        </div>
        <div className="box-body">
          <CustomerFilterForm
            form="customer-filter-form"
            onSubmit={this.handleSearch}
          />
          <hr />
          <ReactTable
            columns={this.columns}
            data={this.state.data}
            pages={this.state.pages}
            loading={this.state.loading}
            pageSize={this.state.limit}
            page={this.state.page - 1}
            onPageChange={this.handleUpdatePage}
            onPageSizeChange={this.handleUpdatePageSize}
            onSortedChange={this.handleUpdateSort}
            pageSizeOptions={[10, 20, 50, 100]}
            className="-striped -highlight"
            manual
            minRows={0}
            PaginationComponent={Pagination}
            SubComponent={row => (
              <CustomerAddress customerId={row.original.id} />
            )}
          />
        </div>
      </div>
    );
  }
}

CustomerList.propTypes = {
  fetch: PropTypes.func.isRequired,
  update: PropTypes.func.isRequired,
};

export default CustomerList;
