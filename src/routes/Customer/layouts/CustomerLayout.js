import React from "react";
import PropTypes from "prop-types";

class CustomerLayout extends React.Component {
  render() {
    return (
      <div className="content-wrapper">
        <section className="content-header">
          <h1>
            Admin
            <small>Customer</small>
          </h1>
        </section>
        <section className="content">{this.props.children}</section>
      </div>
    );
  }
}

CustomerLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default CustomerLayout;
