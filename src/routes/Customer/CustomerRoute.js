import React from "react";
import PropTypes from "prop-types";
import { Route, Switch } from "react-router";

import { ProtectedRoute } from "components/Admin";
import { ROLE_CODE } from "helpers/constants";

import CustomerLayout from "./layouts/CustomerLayout";
import CustomerList from "./components/CustomerListContainer";

const CustomerRoute = ({ match: { url } }) => (
  <CustomerLayout>
    <Switch>
      <Route
        path={`${url}/`}
        exact
        render={routerProps => (
          <ProtectedRoute {...routerProps} allowedRoles={[ROLE_CODE.ADMIN]}>
            <CustomerList {...routerProps} />
          </ProtectedRoute>
        )}
      />
    </Switch>
  </CustomerLayout>
);

CustomerRoute.propTypes = {
  match: PropTypes.object.isRequired,
};

export default CustomerRoute;
