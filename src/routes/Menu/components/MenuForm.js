import React from "react";
import PropTypes from "prop-types";
import { map } from "lodash";
import { Field, reduxForm } from "redux-form";
import { DatePicker, DishSelect, Select } from "components/FormControl";
import { validateRequired } from "helpers/validator";

import { MENU_TYPES, CARD_TYPES } from "helpers/constants";

const menuOptions = map(MENU_TYPES, (label, value) => ({ value, label }));
const typeOptions = map(CARD_TYPES, (label, value) => ({ value, label }));

const MenuForm = ({ handleSubmit, submitting, details }) => (
  <form className="modal-form" onSubmit={handleSubmit}>
    <Field
      component={DatePicker}
      id="day"
      name="day"
      label="Ngày"
      placeholder="DD-MM-YYYY"
      dateFormat="DD-MM-YYYY"
    />
    <Field
      component={Select}
      id="type"
      options={menuOptions}
      name="type"
      label="type"
      placeholder="Chọn loại menu"
    />
    <Field
      component={Select}
      id="size"
      options={typeOptions}
      name="size"
      label="Loại gói"
      placeholder="Chọn loại gói"
    />
    <Field
      component={DishSelect}
      id={"details[0].id"}
      name={"details[0].id"}
      placeholder={details && details[0] ? "details[0].name" : ""}
      label={"Món chính"}
    />
    <Field
      component={DishSelect}
      id={"details[1].id"}
      name={"details[1].id"}
      placeholder={details && details[1] ? "details[1].name" : ""}
      label={"Món phụ"}
    />
    <Field
      component={DishSelect}
      id={"details[2].id"}
      name={"details[2].id"}
      placeholder={details && details[2] ? "details[2].name" : ""}
      label={"Món canh"}
    />
    <Field
      component={DishSelect}
      id={"details[3].id"}
      name={"details[3].id"}
      placeholder={details && details[3] ? "details[3].name" : ""}
      label={"Món rau"}
    />
    <div className="text-right">
      <button type="submit" className="btn btn-success" disabled={submitting}>
        Save
      </button>
    </div>
  </form>
);

MenuForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  details: PropTypes.array,
};

export default reduxForm({
  validate: (values, ownProps) => ({
    day: validateRequired(values.day),
    type: validateRequired(values.type),
    "details[0].id": validateRequired(values.details && values.details[0]),
    "details[1].id": validateRequired(values.details && values.details[1]),
    "details[2].id": validateRequired(values.details && values.details[2]),
  }),
})(MenuForm);
