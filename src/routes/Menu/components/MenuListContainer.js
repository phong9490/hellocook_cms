import { connect } from "react-redux";
import { toast } from "react-toastify";

import * as menuActions from "../menu";

import MenuList from "./MenuList";

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  fetch: params => dispatch(menuActions.fetch(params)),
  create: async (values, callback) => {
    const result = await dispatch(menuActions.create(values));
    if (result && result.menu) {
      const menu = result.menu;
      toast.success(`${menu.day} được tạo thành công`);

      if (callback) {
        callback();
      }
    }
  },
  update: async (values, callback) => {
    const result = await dispatch(menuActions.update(values));
    if (result && result.menu) {
      const menu = result.menu;
      toast.success(`${menu.day} được sửa thành công`);

      if (callback) {
        callback();
      }
    }
  },
  deleteMenu: async (id, callback) => {
    const result = await dispatch(menuActions.deleteMenu(id));
    if (result) {
      toast.success("Xóa menu thành công");

      if (callback) {
        callback();
      }
    }
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(MenuList);
