import React from "react";
import PropTypes from "prop-types";
import { Field, reduxForm } from "redux-form";
import { DateRange } from "components/FormControl";

const MenuFilterForm = ({ handleSubmit, submitting }) => (
  <form className="inline-form" onSubmit={handleSubmit}>
    <Field
      component={DateRange}
      startDateId="startDate"
      endDateId="endDate"
      startDatePlaceholder="Ngày bắt đầu"
      endDatePlaceholder="Ngày kết thúc"
      name="date"
      label="Ngày bán"
      dateFormat="DD-MM-YYYY"
      allowSameDate
    />
    <button type="submit" className="btn btn-default" disabled={submitting}>
      <i className="fa fa-search" /> Tìm kiếm
    </button>
  </form>
);

MenuFilterForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
};

export default reduxForm({
  validate: values => ({}),
})(MenuFilterForm);
