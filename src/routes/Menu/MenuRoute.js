import React from "react";
import PropTypes from "prop-types";
import { Route, Switch } from "react-router";

import { ProtectedRoute } from "components/Admin";
import { ROLE_CODE } from "helpers/constants";

import MenuLayout from "./layouts/MenuLayout";
import MenuList from "./components/MenuListContainer";

const MenuRoute = ({ match: { url } }) => (
  <MenuLayout>
    <Switch>
      <Route
        path={`${url}/`}
        exact
        render={routerProps => (
          <ProtectedRoute {...routerProps} allowedRoles={[ROLE_CODE.ADMIN]}>
            <MenuList {...routerProps} />
          </ProtectedRoute>
        )}
      />
    </Switch>
  </MenuLayout>
);

MenuRoute.propTypes = {
  match: PropTypes.object.isRequired,
};

export default MenuRoute;
