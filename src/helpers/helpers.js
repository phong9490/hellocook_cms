import React from "react";
import { map } from "lodash";
import cx from "classnames";
import moment from "moment";

export const checkOrderItemsInStatus = (orderItems, status) => {
  const items = map(orderItems, oi => oi);
  if (items.length === 0) {
    return false;
  }
  for (const orderItem of items) {
    if (orderItem.status !== status) {
      return false;
    }
  }
  return true;
};

export const renderStatus = status => {
  const className = `label-${status}`;
  return <span className={cx("label", "bg-gray", className)}>{status}</span>;
};

export const renderDate = date => moment(date).format("DD-MM-YYYY");

export const renderDateTime = date =>
  moment(date).format("DD-MM-YYYY HH:mm:ss");

export const renderInvoiceDeliveryInfo = invoice => {
  const { province, district, ward } = invoice;

  return (
    <p>
      <b>{invoice.delivery_address}</b>
      <br />
      {ward.name}, {district.name}, {province.name}
      <br />
      Ngày giao:{" "}
      {moment(invoice.delivery_date, "YYYY-MM-DD").format("DD-MM-YYYY")}
    </p>
  );
};

export const renderInvoiceInfo = invoice => {
  const deliveryDate = invoice.delivery_date;
  const user = invoice.customer.user;

  return (
    <p>
      <b>{invoice.uuid}</b>
      <br />
      {user.full_name}
      <br />
      Ngày giao: {moment(deliveryDate, "YYYY-MM-DD").format("DD-MM-YYYY")}
    </p>
  );
};

export const renderCustomerInfo = customer => {
  const user = customer.user;
  return (
    <p>
      <b>{user.full_name}</b>
      <br />
      {user.phone}
      <br />
      {user.email}
    </p>
  );
};

export const renderOrderItemInfo = orderItem => {
  return (
    <p>
      <b>{orderItem.url}</b>
      <br />
      Size: {orderItem.size}
      <br />
      Màu sắc: {orderItem.variance}
      <br />
      SL: {orderItem.quantity}
      <br />
      Giá: {renderPriceWithCurrency(orderItem.price, orderItem.currency)}
      <br />
      {orderItem.note}
    </p>
  );
};

export const renderPriceWithCurrency = (price, currency) => {
  return (
    price !== null && (
      <span>
        {price}
        {currency.symbol}
      </span>
    )
  );
};

export const renderMoney = price => {
  return Number(price).toLocaleString("vi-VN");
};

export const renderSupplyOrderInfo = supplyOrder => {
  return (
    <p>
      PO: {supplyOrder.po_number}
      <br />
      Tracking: {supplyOrder.tracking_number}
    </p>
  );
};

export const renderAddress = order => {
  return (
    <p>
      {order.delivery_address}
      <br />
      {order.ward.name}, {order.district.name}
      <br />
      {order.province.name}, {order.country.name}
    </p>
  );
};

export const formatTimeOfDay = value => {
  const hour = Math.floor(value / 60);
  const minute = value % 60;

  let message = "";
  if (hour < 10) {
    message = `0${hour}`;
  } else {
    message = hour;
  }
  if (minute < 10) {
    message = `${message}:0${minute}`;
  } else {
    message = `${message}:${minute}`;
  }
  return message;
};
