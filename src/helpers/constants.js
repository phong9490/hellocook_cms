export const ROLE_CODE = {
  ADMIN: 1,
  CUSTOMER: 2,
};

export const ORDER_ITEM_STATUS_CODE = {
  ORDERED: 0,
  ACCEPTED: 1,
  CONFIRMING: 2,
  CONFIRMED: 3,
  CANCELED: 4,
  PURCHASED: 5,
  SUPPLY_DELIVERING: 6,
  WAREHOUSE: 7,
  TRANSFERING: 8,
  CUSTOMER_DELIVERING: 9,
  CUSTOMER_DELIVERED: 10,
  FINISHED: 11,
  RETURNED: 12,
};

export const SUPPLY_ORDER_STATUS_CODE = {
  PURCHASED: 0,
  DELIVERING: 1,
  DELIVERED: 2,
  FINISHED: 3,
};

export const DELIVERY_STATUS_CODE = {
  DELIVERING: 0,
  DELIVERED: 1,
};

export const PAYMENT_STATUS_CODE = {
  UNPAID: 0,
  DEPOSIT: 1,
  FINISHED: 2,
};

export const PAYMENT_TYPE_CODE = {
  DEPOSIT: 1,
  PAYMENT: 2,
};

export const ORDER_ITEM_STATUSES = {
  [ORDER_ITEM_STATUS_CODE.ORDERED]: "Ordered",
  [ORDER_ITEM_STATUS_CODE.ACCEPTED]: "Accepted",
  [ORDER_ITEM_STATUS_CODE.CONFIRMING]: "Confirming",
  [ORDER_ITEM_STATUS_CODE.CONFIRMED]: "Confirmed",
  [ORDER_ITEM_STATUS_CODE.CANCELED]: "Canceled",
  [ORDER_ITEM_STATUS_CODE.PROCESSING]: "Processing",
  [ORDER_ITEM_STATUS_CODE.SUPPLY_DELIVERING]: "Supplier Delivering",
  [ORDER_ITEM_STATUS_CODE.CUSTOMER_DELIVERING]: "Customer Delivering",
  [ORDER_ITEM_STATUS_CODE.CUSTOMER_DELIVERED]: "Customer Delivered",
  [ORDER_ITEM_STATUS_CODE.DELIVERING]: "Delivering",
  [ORDER_ITEM_STATUS_CODE.DELIVERED]: "Delivered",
  [ORDER_ITEM_STATUS_CODE.FINISHED]: "Finished",
  [ORDER_ITEM_STATUS_CODE.RETURNED]: "Returned",
  [ORDER_ITEM_STATUS_CODE.PURCHASED]: "Purchased",
  [ORDER_ITEM_STATUS_CODE.WAREHOUSE]: "Warehouse",
  [ORDER_ITEM_STATUS_CODE.TRANSFERING]: "Transfering",
};

export const SUPPLY_ORDER_STATUSES = {
  [SUPPLY_ORDER_STATUS_CODE.PURCHASED]: "Purchased",
  [SUPPLY_ORDER_STATUS_CODE.DELIVERING]: "Delivering",
  [SUPPLY_ORDER_STATUS_CODE.DELIVERED]: "Delivered",
  [SUPPLY_ORDER_STATUS_CODE.FINISHED]: "Finished",
};

export const DELIVERY_STATUSES = {
  [DELIVERY_STATUS_CODE.DELIVERING]: "Delivering",
  [DELIVERY_STATUS_CODE.DELIVERED]: "Delivered",
};

export const PAYMENT_TYPES = {
  [PAYMENT_TYPE_CODE.DEPOSIT]: "Deposit",
  [PAYMENT_TYPE_CODE.PAYMENT]: "Finish",
};

export const PAYMENT_STATUSES = {
  [PAYMENT_STATUS_CODE.UNPAID]: "Unpaid",
  [PAYMENT_STATUS_CODE.DEPOSIT]: "Deposit",
  [PAYMENT_STATUS_CODE.FINISHED]: "Finished",
};

export const USER_STATUSES = { 0: "Inactive", 1: "Active" };

export const ROLES = {
  [ROLE_CODE.CUSTOMER]: "Customer",
  [ROLE_CODE.ADMIN]: "Admin",
};

export const DISH_TYPES = {
  1: "Chính",
  2: "Phụ",
  3: "Canh",
  4: "Rau",
};

export const MENU_TYPES = {
  1: "Người thường",
  2: "Người bệnh",
};

export const CARD_TYPES = {
  1: "2 người",
  2: "4 người",
};

export const ORDER_STATUS = {
  0: "Đã hủy",
  1: "Đã đặt",
};
