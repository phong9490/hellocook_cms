export const validateRequired = value => {
  if (value === undefined || value === "") {
    return "Required";
  }
  return false;
};

export const validateMatch = (value, match) => {
  if (value !== match) {
    return "Not match";
  }
  return false;
};

export const validateGreaterOrEqual = (value, number) => {
  if (value < number) {
    return `Must be greater of equal than ${number}`;
  }
};

export const validateGreaterThan = (value, number) => {
  if (value <= number) {
    return `Must be greater than ${number}`;
  }
};

export const validateFloatNumber = value => {
  if (value && !/^-?\d+\.?\d*$/.test(value)) {
    return "Invalid number format";
  }
};
