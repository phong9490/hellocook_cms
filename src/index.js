import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { ConnectedRouter } from "react-router-redux";
import { Route } from "react-router-dom";

// import registerServiceWorker from "./registerServiceWorker";
import store, { history } from "./store";
import withPageTracking from "./withPageTracking";
import App from "./App";

import "./index.css";

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Route component={withPageTracking(App)} />
    </ConnectedRouter>
  </Provider>,
  document.getElementById("root"),
);

// registerServiceWorker();
