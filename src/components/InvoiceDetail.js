import React from "react";
import { map } from "lodash";
import PropTypes from "prop-types";

import { renderPriceWithCurrency, renderStatus } from "helpers/helpers";
import { ORDER_ITEM_STATUSES } from "helpers/constants";

class InvoiceDetail extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      invoice: undefined,
      loading: false,
    };
  }

  async handleFetchData() {
    this.setState({
      loading: true,
    });
    const data = await this.props.fetch(this.props.invoiceId);

    if (data && data.invoice) {
      this.setState({
        invoice: data.invoice,
        loading: false,
      });
    } else {
      this.setState({
        loading: false,
      });
    }
  }

  componentDidMount() {
    this.handleFetchData();
  }

  render() {
    const { loading, invoice } = this.state;

    if (loading) {
      return (
        <div className="order-item-detail-container text-center">
          <i className="fa fa-circle-o-notch fa-spin loading-icon" />
        </div>
      );
    }

    if (!invoice) {
      return false;
    }

    return (
      <div className="order-item-detail-container">
        {map(invoice.orders, order => (
          <div className="table-responsive" key={order.id}>
            <p className="heading">
              <b>{order.uuid}</b>
              <br />
              {order.website.name}
            </p>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th colSpan={2}>Link</th>
                  <th>Size</th>
                  <th>Màu</th>
                  <th>SL</th>
                  <th>Giá</th>
                  <th>Trạng thái</th>
                </tr>
              </thead>

              <tbody>
                {map(order.order_items, orderItem => (
                  <tr key={orderItem.id}>
                    <td className="text-center">{orderItem.uuid}</td>
                    <td className="text-center">
                      <div
                        className="thumbnail"
                        style={{
                          backgroundImage: `url(${orderItem.meta_image})`,
                        }}
                      />
                    </td>
                    <td>
                      <a target="_blank" href={orderItem.url}>
                        <b>{orderItem.meta_title}</b>{" "}
                        <i className="fa fa-external-link-square" />
                      </a>
                      <p>{orderItem.url} </p>
                      <p>{orderItem.note} </p>
                    </td>
                    <td className="text-center">{orderItem.size}</td>
                    <td className="text-center">{orderItem.variance}</td>
                    <td className="text-center">{orderItem.quantity}</td>
                    <td className="text-center">
                      {renderPriceWithCurrency(
                        orderItem.price,
                        orderItem.currency,
                      )}
                    </td>
                    <td className="text-center">
                      {renderStatus(ORDER_ITEM_STATUSES[orderItem.status])}
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        ))}
      </div>
    );
  }
}

InvoiceDetail.propTypes = {
  invoiceId: PropTypes.number.isRequired,
  fetch: PropTypes.func.isRequired,
};

export default InvoiceDetail;
