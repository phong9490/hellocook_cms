import { connect } from "react-redux";

import * as orderActions from "routes/Order/order";

import InvoiceDetail from "./InvoiceDetail";

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  fetch: id => dispatch(orderActions.fetchInvoice(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(InvoiceDetail);
