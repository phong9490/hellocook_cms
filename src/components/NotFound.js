import React from "react";
import PropTypes from "prop-types";

const NotFound = ({ location }) => (
  <div className="notfound-page text-center">
    URL <code>{location.pathname}</code> not found on this website
  </div>
);

NotFound.propTypes = {
  location: PropTypes.object.isRequired,
};

export default NotFound;
