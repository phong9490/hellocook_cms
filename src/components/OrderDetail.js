import React from "react";
import { map } from "lodash";
import PropTypes from "prop-types";

import { DISH_TYPES } from "helpers/constants";

class OrderDetail extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      order: undefined,
      loading: false,
    };
  }

  async handleFetchData() {
    this.setState({
      loading: true,
    });
    const data = await this.props.fetch(this.props.orderId);

    if (data && data.detail) {
      this.setState({
        order: data.detail,
        loading: false,
      });
    } else {
      this.setState({
        loading: false,
      });
    }
  }

  componentDidMount() {
    this.handleFetchData();
  }

  render() {
    const { loading, order } = this.state;

    if (loading) {
      return (
        <div className="order-item-detail-container text-center">
          <i className="fa fa-circle-o-notch fa-spin loading-icon" />
        </div>
      );
    }

    if (!order) {
      return false;
    }

    return (
      <div className="order-item-detail-container">
        <div className="table-responsive">
          <table className="table table-striped">
            <thead>
              <tr>
                <th>STT</th>
                <th>Tên món</th>
                <th>Loại món</th>
              </tr>
            </thead>
            <tbody>
              {map(order, (detail, index) => (
                <tr key={index}>
                  <td className="text-center">{index + 1}</td>
                  <td>{detail.name}</td>
                  <td className="text-center">{DISH_TYPES[detail.type]}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

OrderDetail.propTypes = {
  orderId: PropTypes.number.isRequired,
  fetch: PropTypes.func.isRequired,
};

export default OrderDetail;
