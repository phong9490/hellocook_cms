import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

class Dropdown extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
    };

    this.handleToggle = this.handleToggle.bind(this);
    this.assignRef = this.assignRef.bind(this);
    this.handleDocumentClick = this.handleDocumentClick.bind(this);
    this.mounted = false;
  }

  componentDidMount() {
    this.mounted = true;
    document.addEventListener("click", this.handleDocumentClick, false);
  }

  componentWillUnmount() {
    this.mounted = false;
    document.removeEventListener("click", this.handleDocumentClick, false);
  }

  handleDocumentClick(event) {
    if (
      this.mounted &&
      this.dropdown &&
      !this.dropdown.contains(event.target)
    ) {
      this.setState({ isOpen: false });
    }
  }

  assignRef(component) {
    this.dropdown = component;
  }

  handleToggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  render() {
    this.toggleComponent = React.cloneElement(this.props.toggleComponent, {
      onClick: this.handleToggle,
      ref: this.assignRef,
    });

    return (
      <li className={cx(this.props.className, { open: this.state.isOpen })}>
        {this.toggleComponent}
        {this.props.children}
      </li>
    );
  }
}

Dropdown.propTypes = {
  className: PropTypes.string,
  toggleComponent: PropTypes.node.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default Dropdown;
