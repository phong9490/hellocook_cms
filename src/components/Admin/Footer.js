import React from "react";

class Footer extends React.Component {
  render() {
    return (
      <footer className="main-footer">
        <div className="pull-right hidden-xs">
          <b>Version</b> 0.9.0
        </div>
        <strong>
          Copyright © 2018 <a href="https://youngpilots.vn">YoungPilots</a>.
        </strong>{" "}
        All rights reserved.
      </footer>
    );
  }
}

Footer.propTypes = {};

export default Footer;
