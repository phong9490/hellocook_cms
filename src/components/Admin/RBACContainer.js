import { connect } from "react-redux";

import * as selectors from "store/selectors";

import RBAC from "./RBAC";

const mapStateToProps = state => ({
  currentUser: selectors.getCurrentUser(state),
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(RBAC);
