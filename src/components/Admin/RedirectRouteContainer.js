import { connect } from "react-redux";

import * as selectors from "store/selectors";

import RedirectRoute from "./RedirectRoute";

const mapStateToProps = state => ({
  currentUser: selectors.getCurrentUser(state),
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(RedirectRoute);
