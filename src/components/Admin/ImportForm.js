import React from "react";
import PropTypes from "prop-types";
import { Field, reduxForm } from "redux-form";
import { TextInput, WarehouseSelect } from "components/FormControl";

import { validateRequired } from "helpers/validator";

const ImportForm = ({ handleSubmit, submitting, children }) => (
  <form className="modal-form" onSubmit={handleSubmit}>
    <div className="row">
      <div className="col-md-6">
        <Field
          component={WarehouseSelect}
          type="text"
          id="warehouse_id"
          name="warehouse_id"
          label="Warehouse"
        />
      </div>
    </div>
    {children}
    <Field
      component={TextInput}
      type="hidden"
      id="order_items"
      name="order_items"
    />
    <div className="text-right">
      <button type="submit" className="btn btn-success" disabled={submitting}>
        Xác nhận
      </button>
    </div>
  </form>
);

ImportForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default reduxForm({
  validate: values => ({
    warehouse_id: validateRequired(values.warehouse_id),
  }),
})(ImportForm);
