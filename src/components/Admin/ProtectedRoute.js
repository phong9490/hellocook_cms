import React from "react";
import PropTypes from "prop-types";
import { Redirect } from "react-router-dom";

import Forbidden from "../Forbidden";

class ProtectedRoute extends React.Component {
  render() {
    const { currentUser, children, allowedRoles } = this.props;

    if (!currentUser) {
      return <Redirect to="/auth" />;
    }

    if (!allowedRoles || allowedRoles.indexOf(currentUser.role) < 0) {
      return <Forbidden {...this.props} />;
    }
    return children;
  }
}

ProtectedRoute.propTypes = {
  currentUser: PropTypes.object,
  allowedRoles: PropTypes.array.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default ProtectedRoute;
