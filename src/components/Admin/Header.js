import React from "react";
import PropTypes from "prop-types";
import moment from "moment";
import { Link } from "react-router-dom";

import Dropdown from "../Dropdown";

class Header extends React.Component {
  render() {
    const { currentUser } = this.props;

    return (
      <header className="main-header">
        <Link to="/" className="logo">
          <span className="logo-mini">
            <b>H</b>F
          </span>
          <span className="logo-lg">
            <b>Home</b>Fresh
          </span>
        </Link>
        <nav className="navbar navbar-static-top">
          <a
            onClick={this.props.toggleCollapse}
            className="sidebar-toggle"
            role="button"
          >
            <span className="sr-only">Toggle navigation</span>
          </a>

          <div className="navbar-custom-menu">
            <ul className="nav navbar-nav">
              <Dropdown
                className="dropdown messages-menu"
                toggleComponent={
                  <a className="dropdown-toggle">
                    <i className="fa fa-envelope-o" />
                    <span className="label label-success">4</span>
                  </a>
                }
              >
                <ul className="dropdown-menu">
                  <li className="header">You have 4 messages</li>
                  <li>
                    <ul className="menu">
                      <li>
                        <a href={null}>
                          <div className="pull-left">
                            <img
                              src="https://adminlte.io/themes/AdminLTE/dist/img/user4-128x128.jpg"
                              className="img-circle"
                              alt="User Avatar"
                            />
                          </div>
                          <h4>
                            Support Team
                            <small>
                              <i className="fa fa-clock-o" /> 5 mins
                            </small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li className="footer">
                    <a href={null}>See All Messages</a>
                  </li>
                </ul>
              </Dropdown>
              <Dropdown
                className="dropdown notifications-menu"
                toggleComponent={
                  <a className="dropdown-toggle">
                    <i className="fa fa-bell-o" />
                    <span className="label label-warning">10</span>
                  </a>
                }
              >
                <ul className="dropdown-menu">
                  <li className="header">You have 10 notifications</li>
                  <li>
                    <ul className="menu">
                      <li>
                        <a href={null}>
                          <i className="fa fa-users text-aqua" /> 5 new members
                          joined today
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li className="footer">
                    <a href={null}>View all</a>
                  </li>
                </ul>
              </Dropdown>
              <Dropdown
                className="dropdown user user-menu"
                toggleComponent={
                  <a className="dropdown-toggle">
                    <img
                      src="https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg"
                      className="user-image"
                      alt="User pt"
                    />
                    <span className="hidden-xs">{currentUser.full_name}</span>
                  </a>
                }
              >
                <ul className="dropdown-menu">
                  <li className="user-header">
                    <img
                      src="https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg"
                      className="img-circle"
                      alt="User Logo"
                    />
                    <p>
                      {currentUser.email}
                      <small>
                        Member since{" "}
                        {moment(currentUser.created_at).format("DD/MM/YYYY")}
                      </small>
                    </p>
                  </li>
                  <li className="user-body">
                    <div className="row">
                      <div className="col-xs-4 text-center">
                        <a href={null}>Followers</a>
                      </div>
                      <div className="col-xs-4 text-center">
                        <a href={null}>Sales</a>
                      </div>
                      <div className="col-xs-4 text-center">
                        <a href={null}>Friends</a>
                      </div>
                    </div>
                  </li>
                  <li className="user-footer">
                    <div className="pull-left">
                      <a href={null} className="btn btn-default">
                        Profile
                      </a>
                    </div>
                    <div className="pull-right">
                      <a
                        onClick={this.props.logout}
                        className="btn btn-default"
                      >
                        Sign out
                      </a>
                    </div>
                  </li>
                </ul>
              </Dropdown>
              <li>
                <a onClick={this.props.logout}>
                  <i className="fa fa-power-off" />
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
    );
  }
}

Header.propTypes = {
  currentUser: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired,
  toggleCollapse: PropTypes.func.isRequired,
};

export default Header;
