import React from "react";
import PropTypes from "prop-types";

class RBAC extends React.Component {
  render() {
    const { currentUser, children, allowedRoles } = this.props;

    if (
      !currentUser ||
      !allowedRoles ||
      allowedRoles.indexOf(currentUser.role) < 0
    ) {
      return false;
    }
    return children;
  }
}

RBAC.propTypes = {
  currentUser: PropTypes.object,
  allowedRoles: PropTypes.array.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default RBAC;
