import { connect } from "react-redux";

import * as selectors from "store/selectors";

import ProtectedRoute from "./ProtectedRoute";

const mapStateToProps = state => ({
  currentUser: selectors.getCurrentUser(state),
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ProtectedRoute);
