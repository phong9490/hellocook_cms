import React from "react";
import PropTypes from "prop-types";
import { Field, reduxForm } from "redux-form";
import { TextInput } from "components/FormControl";

const ReviewForm = ({ form, handleSubmit, submitting, children }) => (
  <form className="modal-form" onSubmit={handleSubmit}>
    {children}
    <Field
      component={TextInput}
      type="hidden"
      id="order_items"
      name="order_items"
    />
    <div className="text-right">
      <button type="submit" className="btn btn-success" disabled={submitting}>
        Xác nhận
      </button>
    </div>
  </form>
);

ReviewForm.propTypes = {
  form: PropTypes.string.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default reduxForm({
  validate: values => ({}),
})(ReviewForm);
