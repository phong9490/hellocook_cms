import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

class SidebarTreeview extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
    };

    this.handleToggle = this.handleToggle.bind(this);
  }

  componentDidMount() {
    this.mounted = true;
    if (this.props.toggleComponent) {
      this.toggleComponent = React.cloneElement(this.props.toggleComponent, {
        ref: this.assignRef,
        onClick: this.handleToggle,
      });
    }
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  handleToggle() {
    if (this.props.children) {
      this.setState({
        isOpen: !this.state.isOpen,
      });
    }
  }

  render() {
    return (
      <li
        className={cx(this.props.className, {
          "menu-open": this.state.isOpen,
        })}
      >
        {this.toggleComponent}
        {this.props.children && (
          <ul className={cx("treeview-menu", { show: this.state.isOpen })}>
            {this.props.children}
          </ul>
        )}
      </li>
    );
  }
}

SidebarTreeview.propTypes = {
  toggleComponent: PropTypes.node.isRequired,
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default SidebarTreeview;
