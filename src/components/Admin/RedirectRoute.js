import React from "react";
import PropTypes from "prop-types";
import { Redirect } from "react-router-dom";

class RedirectRoute extends React.Component {
  render() {
    const { currentUser, children, redirectRoles } = this.props;

    if (currentUser && redirectRoles.indexOf(currentUser.role) >= 0) {
      return <Redirect to="/admin" />;
    }
    return children;
  }
}

RedirectRoute.propTypes = {
  currentUser: PropTypes.object,
  redirectRoles: PropTypes.array.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default RedirectRoute;
