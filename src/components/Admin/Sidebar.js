import React from "react";
import SidebarTreeview from "./SidebarTreeview";
import { NavLink } from "react-router-dom";

import { ROLE_CODE } from "helpers/constants";

import RBAC from "./RBACContainer";

class Sidebar extends React.Component {
  render() {
    return (
      <aside className="main-sidebar">
        <section className="sidebar">
          <ul className="sidebar-menu tree">
            <li>
              <NavLink to="/admin" exact>
                <i className="fa fa-dashboard" /> <span>Trang chủ</span>
              </NavLink>
            </li>
            <RBAC allowedRoles={[ROLE_CODE.BUYER]}>
              <SidebarTreeview
                className="treeview"
                toggleComponent={
                  <a>
                    <i className="fa fa-link" /> <span>Buyer</span>
                  </a>
                }
              >
                <li>
                  <NavLink to="/admin/buyer/accept">
                    <i className="fa fa-calendar-check-o" />{" "}
                    <span>Nhận yêu cầu</span>
                  </NavLink>
                </li>
                <li>
                  <NavLink to="/admin/buyer/order-item">
                    <i className="fa fa-list" /> <span>Quản lý yêu cầu</span>
                  </NavLink>
                </li>
                <li>
                  <NavLink to="/admin/buyer/supply-order">
                    <i className="fa fa-truck" /> <span>Quản lý đơn NCC</span>
                  </NavLink>
                </li>
              </SidebarTreeview>
            </RBAC>
            <RBAC allowedRoles={[ROLE_CODE.ADMIN]}>
              <SidebarTreeview
                className="treeview"
                toggleComponent={
                  <NavLink to="/admin/user">
                    <i className="fa fa-users" /> <span>Tài khoản</span>
                  </NavLink>
                }
              />
            </RBAC>
            <RBAC allowedRoles={[ROLE_CODE.ADMIN]}>
              <SidebarTreeview
                className="treeview"
                toggleComponent={
                  <NavLink to="/admin/customer">
                    <i className="fa fa-user-o" /> <span>Khách hàng</span>
                  </NavLink>
                }
              />
            </RBAC>
            <RBAC allowedRoles={[ROLE_CODE.ADMIN]}>
              <SidebarTreeview
                className="treeview"
                toggleComponent={
                  <NavLink to="/admin/card">
                    <i className="fa fa-credit-card" /> <span>Card</span>
                  </NavLink>
                }
              />
            </RBAC>
            <RBAC allowedRoles={[ROLE_CODE.ADMIN]}>
              <SidebarTreeview
                className="treeview"
                toggleComponent={
                  <NavLink to="/admin/dish">
                    <i className="fa fa-cutlery" /> <span>Món</span>
                  </NavLink>
                }
              />
            </RBAC>
            <RBAC allowedRoles={[ROLE_CODE.ADMIN]}>
              <SidebarTreeview
                className="treeview"
                toggleComponent={
                  <NavLink to="/admin/menu">
                    <i className="fa fa-list" /> <span>Menu</span>
                  </NavLink>
                }
              />
            </RBAC>
            <RBAC allowedRoles={[ROLE_CODE.ADMIN]}>
              <SidebarTreeview
                className="treeview"
                toggleComponent={
                  <a>
                    <i className="fa fa-shopping-cart" /> Đơn hàng
                  </a>
                }
              >
                <li>
                  <NavLink to="/admin/order" exact>
                    <i className="fa fa-file-text-o" /> <span>Đơn hàng</span>
                  </NavLink>
                </li>
                <li>
                  <NavLink to="/admin/order/review" exact>
                    <i className="fa fa-tags" /> <span>Review</span>
                  </NavLink>
                </li>
              </SidebarTreeview>
            </RBAC>
          </ul>
        </section>
      </aside>
    );
  }
}

Sidebar.propTypes = {};

export default Sidebar;
