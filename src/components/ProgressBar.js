import React from "react";
import PropTypes from "prop-types";

class ProgressBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      percent: 0,
    };
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      const percent = this.state.percent + 5;
      if (percent > 100) {
        clearInterval(this.interval);
      }
      this.setState({ percent });
    }, 200);
  }

  componentDidUpdate(prevProps) {
    if (this.props.loading === false) {
      clearInterval(this.interval);
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return this.props.loading ? (
      <div className="progress progress-sm active">
        <div
          className="progress-bar progress-bar-success progress-bar-striped"
          style={{ width: `${this.state.percent}%` }}
        />
      </div>
    ) : (
      false
    );
  }
}

ProgressBar.propTypes = {
  loading: PropTypes.bool.isRequired,
};

export default ProgressBar;
