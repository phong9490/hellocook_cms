import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

class Pagination extends React.Component {
  constructor(props) {
    super(props);
    this.changePage = this.changePage.bind(this);
  }

  getVisiblePages(activePage, totalPage) {
    const delta = 3;
    const left = activePage - delta;
    const right = activePage + delta + 1;
    const range = [];
    const rangeWithDots = [];
    let l = 0;

    for (let i = 1; i <= totalPage; i++) {
      if (i === 1 || i === totalPage || (i >= left && i < right)) {
        range.push(i);
      }
    }

    for (let i of range) {
      if (l) {
        if (i - l === 2) {
          rangeWithDots.push(l + 1);
        } else if (i - l !== 1) {
          rangeWithDots.push("...");
        }
      }
      rangeWithDots.push(i);
      l = i;
    }

    return rangeWithDots;
  }

  changePage(page) {
    const activePage = this.props.page + 1;

    if (page === activePage) {
      return;
    }

    this.props.onPageChange(page - 1);
  }

  render() {
    const { page, pages } = this.props;
    const activePage = page + 1;
    const visiblePages = this.getVisiblePages(activePage, pages);

    return (
      <ul className="pagination no-margin pagination-sm pull-right">
        {visiblePages.map((page, index) => {
          if (page !== "...") {
            return (
              <li key={index} className={cx({ active: page === activePage })}>
                <a onClick={() => this.changePage(page)}>{page}</a>
              </li>
            );
          }
          return (
            <li>
              <a key={index} disabled>
                ...
              </a>
            </li>
          );
        })}
      </ul>
    );
  }
}

Pagination.propTypes = {
  pages: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
};

export default Pagination;
