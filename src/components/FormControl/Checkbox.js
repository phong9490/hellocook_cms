import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import "./Checkbox.css";

class Checkbox extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: props.input.value || false,
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (this.props.input.value !== prevProps.input.value) {
      this.updateInputValue();
    }
  }

  updateInputValue() {
    this.setState({
      value: this.props.input.value || false,
    });
  }

  async handleChange() {
    await this.setState({
      value: !this.state.value,
    });
    this.props.input.onChange(this.state.value);
  }

  render() {
    const { id, label, meta: { touched, error } } = this.props;

    return (
      <div
        className={cx("form-group", "form-checkbox", {
          "has-error": touched && error,
          "has-success": touched && !error,
        })}
      >
        <div id={id} className="checkbox" onClick={this.handleChange}>
          {this.state.value && <i className="fa fa-check" />}
        </div>
        {label && <label htmlFor={id}>{label}</label>}
        {touched &&
          error && <span className="form-text text-danger">{error}</span>}
      </div>
    );
  }
}

Checkbox.propTypes = {
  id: PropTypes.string.isRequired,
  input: PropTypes.object.isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }),
  label: PropTypes.string,
};

export default Checkbox;
