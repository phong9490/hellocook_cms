import React from "react";
import PropTypes from "prop-types";
import Select from "react-select";
import { map } from "lodash";
import cx from "classnames";

import http from "helpers/http";

class CurrencySelect extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: props.input.value,
      options: [],
      isLoading: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.loadOptions = this.loadOptions.bind(this);
  }

  componentDidMount() {
    this.mounted = true;
    this.loadOptions();
  }

  componentDidUpdate(prevProps) {
    if (this.props.input.value !== prevProps.input.value) {
      this.updateInputValue();
    }
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  updateInputValue() {
    this.setState({
      value: this.props.input.value,
    });
  }

  async loadOptions() {
    this.setState({ isLoading: true });

    const response = await http.get("currencies");

    if (response && this.mounted) {
      this.setState({
        isLoading: false,
        options: map(response.data, item => this.convertToOption(item)),
      });
    }
  }
  convertToOption(data) {
    return {
      value: data.id,
      fullData: data,
    };
  }

  handleChange(value) {
    const { input, callback } = this.props;
    this.setState({ value });

    input.onChange(value ? value.value : "");

    if (callback) {
      callback(value);
    }
  }

  renderOption({ fullData }) {
    return (
      <div className="select-option">
        <div className="select-option-value">{fullData.name}</div>
        <div className="select-option-description">
          1{fullData.symbol} ={" "}
          {Math.round(fullData.exchange_rate).toLocaleString("en-US")}đ
        </div>
      </div>
    );
  }

  renderValue({ fullData }) {
    return (
      <div className="select-option">
        <div className="select-option-value">{fullData.name}</div>
      </div>
    );
  }

  render() {
    const { id, label, placeholder, meta: { touched, error } } = this.props;

    return (
      <div
        className={cx("form-group", {
          "has-error": touched && error,
          "has-success": touched && !error,
        })}
      >
        {label && (
          <label htmlFor={id} className="control-label">
            {label}
          </label>
        )}
        <Select
          id={id}
          placeholder={placeholder}
          optionRenderer={this.renderOption}
          valueRenderer={this.renderValue}
          value={this.state.value}
          onChange={this.handleChange}
          options={this.state.options}
          isLoading={this.state.isLoading}
          searchable={false}
          trimFilter
          filterOptions={options => options}
        />
        {touched &&
          error && <span className="form-text text-danger">{error}</span>}
      </div>
    );
  }
}

CurrencySelect.propTypes = {
  id: PropTypes.string.isRequired,
  input: PropTypes.object.isRequired,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }),
  callback: PropTypes.func,
};

export default CurrencySelect;
