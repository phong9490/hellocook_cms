import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import Select from "react-select";

import "./Select.css";

class MySelect extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: props.input.value,
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(selectedOption) {
    const { input } = this.props;
    this.setState({ value: selectedOption });
    input.onChange(selectedOption ? selectedOption.value : "");
  }

  render() {
    const {
      id,
      options,
      label,
      placeholder,
      meta: { touched, error },
    } = this.props;

    return (
      <div
        className={cx("form-group", {
          "has-error": touched && error,
          "has-success": touched && !error,
        })}
      >
        {label && (
          <label htmlFor={id} className="control-label">
            {label}
          </label>
        )}
        <Select
          id={id}
          placeholder={placeholder}
          value={this.state.value}
          onChange={this.handleChange}
          onInputChange={this.handleInputChange}
          options={options}
          isLoading={this.state.isLoading}
          trimFilter
        />
        {touched &&
          error && <span className="form-text text-danger">{error}</span>}
      </div>
    );
  }
}

MySelect.propTypes = {
  id: PropTypes.string.isRequired,
  input: PropTypes.object.isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }),
  label: PropTypes.string,
  placeholder: PropTypes.string,
  options: PropTypes.array.isRequired,
};

export default MySelect;
