import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import { map, filter } from "lodash";
import http from "helpers/http";
import ProgressBar from "components/ProgressBar";

import "./FileUpload.css";

class FileUpload extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      files: props.input.value || [],
      isLoading: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.assignRef = this.assignRef.bind(this);
  }

  assignRef(element) {
    this.inputElement = element;
  }

  componentDidUpdate(prevProps) {
    if (this.props.input.value !== prevProps.input.value) {
      this.updateInputValueToState();
    }
  }

  updateInputValueToState() {
    this.setState({
      files: this.props.input.value || [],
    });
  }

  async uploadFile(file) {
    const formData = new FormData();
    formData.append("file", file);

    const result = await http.post("files/upload", formData, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });

    return result;
  }

  onChange(files) {
    this.setState({ files });
    this.props.input.onChange(files);
  }

  async handleChange(event) {
    if (event.target.files.length > 0) {
      const files = this.props.multi ? [...this.state.files] : [];
      this.setState({ isLoading: true });
      for (const localFile of event.target.files) {
        const data = await this.uploadFile(localFile);
        files.push({
          file_name: localFile.name,
          url: data.url,
        });
        this.onChange(files);
      }

      this.inputElement.value = null;
      this.setState({ isLoading: false });
    }
  }

  handleDeleteFile(file) {
    const files = filter(this.state.files, f => f.url !== file.url);
    this.onChange(files);
  }

  render() {
    const { id, label, input, multi, meta: { touched, error } } = this.props;

    return (
      <div
        className={cx("form-group", {
          "has-error": touched && error,
          "has-success": touched && !error,
        })}
      >
        {label && (
          <label htmlFor={id} className="control-label">
            {label}
          </label>
        )}
        <div className="file-upload">
          <input
            id={id}
            type="file"
            onChange={this.handleChange}
            multiple={multi}
            ref={this.assignRef}
          />
          {this.state.isLoading && (
            <ProgressBar loading={this.state.isLoading} />
          )}
          <div className="file-preview-container">
            {map(this.state.files, (file, index) => (
              <div key={index} className="file-preview">
                <i className="file-icon fa fa-file-image-o" />
                <span>{file.file_name}</span>
                <button
                  type="button"
                  className="delete-btn"
                  onClick={() => this.handleDeleteFile(file)}
                >
                  &times;
                </button>
              </div>
            ))}
          </div>
        </div>

        {touched &&
          error && <span className="form-text text-danger">{error}</span>}
      </div>
    );
  }
}

FileUpload.propTypes = {
  id: PropTypes.string.isRequired,
  input: PropTypes.object.isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }),
  label: PropTypes.string,
  multi: PropTypes.bool,
};

export default FileUpload;
