import React from "react";
import PropTypes from "prop-types";
import { DateRangePicker } from "react-dates";
import moment from "moment";
import cx from "classnames";

import "./DateRange.css";

class DateRange extends React.Component {
  constructor(props) {
    super(props);

    const initialValues = props.input.value;
    const { startDate, endDate } = initialValues;

    this.state = {
      focusedInput: undefined,
      startDate: startDate ? moment(startDate, props.dateFormat) : undefined,
      endDate: endDate ? moment(endDate, props.dateFormat) : undefined,
    };

    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleFocusChange = this.handleFocusChange.bind(this);
    this.isOutsideRange = this.isOutsideRange.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (this.props.input.value !== prevProps.input.value) {
      this.updateInputValue();
    }
  }

  updateInputValue() {
    const initialValues = this.props.input.value;
    const { dateFormat } = this.props;

    const { startDate, endDate } = initialValues;
    this.setState({
      startDate: startDate ? moment(startDate, dateFormat) : undefined,
      endDate: endDate ? moment(endDate, dateFormat) : undefined,
    });
  }

  async handleDateChange({ startDate, endDate }) {
    const { dateFormat, input, callback } = this.props;

    await this.setState({ startDate, endDate });
    const selectedValue = {
      startDate: startDate
        ? startDate.startOf("day").format(dateFormat)
        : undefined,
      endDate: endDate ? endDate.startOf("day").format(dateFormat) : undefined,
    };
    await input.onChange(selectedValue);
    if (callback) {
      callback(selectedValue);
    }
  }

  handleFocusChange(focusedInput) {
    this.setState({ focusedInput });
  }

  isOutsideRange(date) {
    if (date.isBefore(moment().startOf("day")) && this.props.disallowPastDate) {
      return true;
    }
    if (date.isAfter(moment().endOf("day")) && this.props.disallowFutureDate) {
      if (date.isAfter(moment().endOf("day"))) {
        return true;
      }
      return false;
    }
    return false;
  }

  render() {
    const { startDate, endDate, focusedInput } = this.state;
    const {
      label,
      startDateId,
      endDateId,
      startDatePlaceholder,
      endDatePlaceholder,
      allowSameDate,
      dateFormat,
      meta: { touched, error },
    } = this.props;

    return (
      <div
        className={cx("form-group", {
          "has-error": touched && error,
          "has-success": touched && !error,
        })}
      >
        {label && <label className="control-label">{label}</label>}
        <DateRangePicker
          onDatesChange={this.handleDateChange}
          onFocusChange={this.handleFocusChange}
          focusedInput={focusedInput}
          startDate={startDate}
          startDateId={startDateId}
          startDatePlaceholderText={startDatePlaceholder}
          endDateId={endDateId}
          endDate={endDate}
          endDatePlaceholderText={endDatePlaceholder}
          renderDayContents={this.renderDayContents}
          displayFormat={dateFormat}
          isOutsideRange={this.isOutsideRange}
          minimumNights={allowSameDate ? 0 : 1}
        />
        {touched &&
          error && <span className="form-text text-danger">{error}</span>}
      </div>
    );
  }
}

DateRange.propTypes = {
  startDateId: PropTypes.string.isRequired,
  endDateId: PropTypes.string.isRequired,
  input: PropTypes.object.isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }),
  label: PropTypes.string,
  startDatePlaceholder: PropTypes.string,
  endDatePlaceholder: PropTypes.string,
  callback: PropTypes.func,
  allowSameDate: PropTypes.bool,
  disallowPastDate: PropTypes.bool,
  disallowFutureDate: PropTypes.bool,
  dateFormat: PropTypes.string.isRequired,
};

export default DateRange;
