import React from "react";
import PropTypes from "prop-types";
import Select from "react-select";
import { debounce, map } from "lodash";
import cx from "classnames";

import http from "helpers/http";

class RegionSelect extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: props.input.value,
      options: [],
      isLoading: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.loadOptions = debounce(this.loadOptions.bind(this), 600);
  }

  componentDidMount() {
    this.mounted = true;
    if (!this.props.disabled) {
      this.loadOptions();
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.input.value !== prevProps.input.value) {
      this.updateInputValue();
    }
    if (this.props.parentId !== prevProps.parentId) {
      this.loadOptions();
      if (prevProps.parentId > 0) {
        this.handleChange(undefined);
      }
    }
  }

  componentWillUnmount() {
    this.mounted = false;
  }

  updateInputValue() {
    this.setState({
      value: this.props.input.value,
    });
  }

  async loadOptions(keyword) {
    this.setState({ isLoading: true });

    const response = await http.get("regions", {
      params: {
        filters: {
          keyword,
          parent_id: this.props.parentId,
        },
        limit: 50,
      },
    });

    if (response && this.mounted) {
      this.setState({
        isLoading: false,
        options: map(response.data, item => this.convertToOption(item)),
      });
    }
  }

  convertToOption(data) {
    return {
      value: data.id,
      fullData: data,
    };
  }

  handleSearch(value) {
    if (value) {
      this.loadOptions(value);
    }
    return value;
  }

  handleChange(value) {
    const { input, callback } = this.props;
    this.setState({ value });

    input.onChange(value ? value.value : "");

    if (callback) {
      callback(value);
    }
  }

  renderOption({ fullData }) {
    return <div className="select-option">{fullData.name}</div>;
  }

  renderValue({ fullData }) {
    return <div className="select-option">{fullData.name}</div>;
  }

  render() {
    const {
      id,
      label,
      disabled,
      placeholder,
      meta: { touched, error },
    } = this.props;

    return (
      <div
        className={cx("form-group", {
          "has-error": touched && error,
          "has-success": touched && !error,
        })}
      >
        {label && (
          <label htmlFor={id} className="control-label">
            {label}
          </label>
        )}
        <Select
          id={id}
          placeholder={placeholder}
          optionRenderer={this.renderOption}
          valueRenderer={this.renderValue}
          value={this.state.value}
          onChange={this.handleChange}
          onInputChange={this.handleSearch}
          options={this.state.options}
          isLoading={this.state.isLoading}
          trimFilter
          disabled={disabled}
          filterOptions={options => options}
        />
        {touched &&
          error && <span className="form-text text-danger">{error}</span>}
      </div>
    );
  }
}

RegionSelect.propTypes = {
  id: PropTypes.string.isRequired,
  input: PropTypes.object.isRequired,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  parentId: PropTypes.number,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }),
  callback: PropTypes.func,
  disabled: PropTypes.bool,
};

export default RegionSelect;
