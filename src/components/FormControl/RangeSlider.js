import React from "react";
import PropTypes from "prop-types";
import InputRange from "react-input-range";
import cx from "classnames";
import { debounce } from "lodash";

import "./RangeSlider.css";

class RangeSlider extends React.Component {
  constructor(props) {
    super(props);

    const { input, multi, minValue, maxValue } = props;
    const { min, max } = input.value;

    if (multi) {
      this.state = {
        value: {
          min: min || minValue,
          max: max || maxValue,
        },
      };
    } else {
      this.state = {
        value: input.value || minValue,
      };
    }

    this.debouncedOnChange = debounce(this.onChange.bind(this), 400);
    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    // reset value
    if (
      nextProps.minValue !== this.props.minValue ||
      nextProps.maxValue !== this.props.maxValue
    ) {
      this.handleChange({ min: nextProps.minValue, max: nextProps.maxValue });
    }
  }

  onChange(value) {
    const { input, callback } = this.props;

    input.onChange(value);
    if (callback) {
      callback(value);
    }
  }

  handleChange(value) {
    this.setState({ value });
    this.debouncedOnChange(value);
  }

  render() {
    const {
      id,
      meta: { touched, error },
      multi,
      maxValue,
      minValue,
      label,
      step,
      formatLabel,
    } = this.props;

    return (
      <div
        className={cx("form-group", {
          "has-error": touched && error,
          "has-success": touched && !error,
        })}
      >
        {label && (
          <label htmlFor={id} className="control-label">
            {label}
          </label>
        )}
        <div className={cx("input-range-container", { multi, single: !multi })}>
          <InputRange
            id={id}
            minValue={minValue}
            maxValue={maxValue}
            step={step}
            value={this.state.value}
            onChange={this.handleChange}
            formatLabel={formatLabel}
          />
        </div>
        {touched &&
          error && <span className="form-text text-danger">{error}</span>}
      </div>
    );
  }
}

RangeSlider.propTypes = {
  id: PropTypes.string.isRequired,
  multi: PropTypes.bool,
  input: PropTypes.object.isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }),

  callback: PropTypes.func,
  label: PropTypes.string.isRequired,
  minValue: PropTypes.number.isRequired,
  maxValue: PropTypes.number.isRequired,
  step: PropTypes.number,
  formatLabel: PropTypes.func,
};

export default RangeSlider;
