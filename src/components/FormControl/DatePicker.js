import React from "react";
import PropTypes from "prop-types";
import { SingleDatePicker } from "react-dates";
import moment from "moment";
import cx from "classnames";

import "./DatePicker.css";

class DatePicker extends React.Component {
  constructor(props) {
    super(props);

    const initialValues = props.input.value;

    this.state = {
      focused: false,
      date: initialValues ? moment(initialValues, props.dateFormat) : undefined,
    };

    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleFocusChange = this.handleFocusChange.bind(this);
    this.isOutsideRange = this.isOutsideRange.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (this.props.input.value !== prevProps.input.value) {
      this.updateInputValue();
    }
  }

  updateInputValue() {
    const initialValues = this.props.input.value;
    const { dateFormat } = this.props;
    this.setState({
      date: initialValues ? moment(initialValues, dateFormat) : undefined,
    });
  }

  async handleDateChange(date) {
    const { dateFormat, input, callback } = this.props;

    await this.setState({ date });

    const selectedValue = date
      ? date.startOf("day").format(dateFormat)
      : undefined;
    await input.onChange(selectedValue);
    if (callback) {
      callback(selectedValue);
    }
  }

  handleFocusChange({ focused }) {
    this.setState({ focused });
  }

  isOutsideRange(date) {
    if (date.isBefore(moment().startOf("day")) && this.props.disallowPastDate) {
      return true;
    }
    if (date.isAfter(moment().endOf("day")) && this.props.disallowFutureDate) {
      if (date.isAfter(moment().endOf("day"))) {
        return true;
      }
      return false;
    }
    return false;
  }

  render() {
    const { focused, date } = this.state;
    const {
      label,
      placeholder,
      dateFormat,
      meta: { touched, error },
    } = this.props;
    return (
      <div
        className={cx("form-group", {
          "has-error": touched && error,
          "has-success": touched && !error,
        })}
      >
        {label && <label className="control-label">{label}</label>}
        <SingleDatePicker
          onDateChange={this.handleDateChange}
          onFocusChange={this.handleFocusChange}
          focused={focused}
          date={date}
          placeholder={placeholder}
          displayFormat={dateFormat}
          isOutsideRange={this.isOutsideRange}
        />
        {touched &&
          error && <span className="form-text text-danger">{error}</span>}
      </div>
    );
  }
}

DatePicker.propTypes = {
  input: PropTypes.object.isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }),
  label: PropTypes.string,
  placeholder: PropTypes.string,
  callback: PropTypes.func,
  disallowPastDate: PropTypes.bool,
  disallowFutureDate: PropTypes.bool,
  dateFormat: PropTypes.string.isRequired,
};

export default DatePicker;
