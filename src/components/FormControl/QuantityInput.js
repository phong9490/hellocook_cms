import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

import "./QuantityInput.css";

class QuantityInput extends React.Component {
  constructor(props) {
    super(props);

    this.handleIncrease = this.handleIncrease.bind(this);
    this.handleDecrease = this.handleDecrease.bind(this);
  }

  handleIncrease() {
    const value = Number(this.props.input.value);
    this.props.input.onChange(value + 1);
  }

  handleDecrease() {
    const value = Number(this.props.input.value);
    this.props.input.onChange(value - 1);
  }

  render() {
    const { id, label, input, meta: { touched, error } } = this.props;

    return (
      <div
        className={cx("form-group", {
          "has-error": touched && error,
          "has-success": touched && !error,
        })}
      >
        {label && (
          <label htmlFor={id} className="control-label">
            {label}
          </label>
        )}
        <div className="quantity-input input-group">
          <div className="input-group-btn">
            <button
              type="button"
              className="btn decrement-btn btn-default"
              onClick={this.handleDecrease}
            >
              <i className="fa fa-minus" />
            </button>
          </div>
          <input className="form-control" {...this.props} {...input} />
          <div className="input-group-btn">
            <button
              type="button"
              className="btn increment-btn btn-default"
              onClick={this.handleIncrease}
            >
              <i className="fa fa-plus" />
            </button>
          </div>
        </div>
        {touched &&
          error && <span className="form-text text-danger">{error}</span>}
      </div>
    );
  }
}

QuantityInput.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  input: PropTypes.object.isRequired,
  meta: PropTypes.shape({
    touched: PropTypes.bool,
    error: PropTypes.string,
  }),
  label: PropTypes.string,
};

export default QuantityInput;
