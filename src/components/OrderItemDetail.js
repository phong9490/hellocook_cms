import React from "react";
import PropTypes from "prop-types";

import { renderPriceWithCurrency } from "helpers/helpers";

import "./OrderItemDetail.css";

class OrderItemDetail extends React.Component {
  render() {
    const { orderItem } = this.props;

    return (
      <div className="order-item-detail-container">
        <div className="table-responsive">
          <table className="table table-striped">
            <thead>
              <tr>
                <th colSpan={2}>Link</th>
                <th>Size</th>
                <th>Màu</th>
                <th>SL</th>
                <th>Giá</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td className="text-center">
                  <div
                    className="thumbnail"
                    style={{
                      backgroundImage: `url(${orderItem.meta_image})`,
                    }}
                  />
                </td>
                <td>
                  <a target="_blank" href={orderItem.url}>
                    <b>{orderItem.meta_title}</b>{" "}
                    <i className="fa fa-external-link-square" />
                  </a>
                  <p>{orderItem.url} </p>
                  <p>{orderItem.note} </p>
                </td>
                <td className="text-center">
                  {orderItem.request_size !== orderItem.size ? (
                    <p>
                      <span>{orderItem.request_size}</span>
                      <i className="fa fa-long-arrow-right" />
                      <b>{orderItem.size}</b>
                    </p>
                  ) : (
                    orderItem.size
                  )}
                </td>
                <td className="text-center">
                  {orderItem.request_variance !== orderItem.variance ? (
                    <p>
                      <span>{orderItem.request_variance}</span>
                      <i className="fa fa-long-arrow-right" />
                      <b>{orderItem.variance}</b>
                    </p>
                  ) : (
                    orderItem.variance
                  )}
                </td>
                <td className="text-center">
                  {orderItem.request_quantity !== orderItem.quantity ? (
                    <p>
                      <span>{orderItem.request_quantity}</span>
                      <i className="fa fa-long-arrow-right" />
                      <b>{orderItem.quantity}</b>
                    </p>
                  ) : (
                    orderItem.quantity
                  )}
                </td>
                <td className="text-center">
                  {orderItem.request_price !== orderItem.price ? (
                    <p>
                      <span>
                        {renderPriceWithCurrency(
                          orderItem.request_price,
                          orderItem.currency,
                        )}
                      </span>
                      <i className="fa fa-long-arrow-right" />
                      <b>
                        {renderPriceWithCurrency(
                          orderItem.price,
                          orderItem.currency,
                        )}
                      </b>
                    </p>
                  ) : (
                    renderPriceWithCurrency(orderItem.price, orderItem.currency)
                  )}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

OrderItemDetail.propTypes = {
  orderItem: PropTypes.object.isRequired,
};

export default OrderItemDetail;
