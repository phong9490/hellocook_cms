import { connect } from "react-redux";

import * as orderActions from "routes/Order/order";

import OrderDetail from "./OrderDetail";

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  fetch: id => dispatch(orderActions.fetchOrder(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderDetail);
