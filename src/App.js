import React from "react";
import { ToastContainer } from "react-toastify";
import "react-dates/initialize";
import Modal from "react-modal";

import CoreLayout from "./layouts/CoreLayoutContainer";
import Routes from "./routes";

Modal.setAppElement("body");

class App extends React.Component {
  render() {
    return (
      <CoreLayout {...this.props}>
        <Routes />
        <ToastContainer
          hideProgressBar
          pauseOnHover={false}
          pauseOnFocusLoss={false}
          autoClose={3000}
        />
      </CoreLayout>
    );
  }
}

App.propTypes = {};

export default App;
