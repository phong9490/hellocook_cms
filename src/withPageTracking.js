import React from "react";
import ReactGA from "react-ga";

// Init Google Analytics
ReactGA.initialize(process.env.REACT_APP_GA_ID);

function trackPage({ pathname }) {
  ReactGA.set({ page: pathname });
  ReactGA.pageview(pathname);
}

/**
 * HOC that adds page tracking via GA to whatever.
 */
const withPageTracking = Component => props => {
  if (props.location) {
    trackPage(props.location);
  }

  return <Component {...props} />;
};

export default withPageTracking;
