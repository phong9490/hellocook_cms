import http from "helpers/http";

const UPDATE_CURRENT_USER = "auth/UPDATE_CURRENT_USER";
const UPDATE_TOKEN = "auth/UPDATE_TOKEN";
const SHOW_LOGIN_MODAL = "auth/SHOW_LOGIN";
const LOGOUT = "auth/LOGOUT";

export const initialState = {
  token: undefined,
  currentUser: undefined,
  isLoginModalShowing: false,
};

export const showLoginModal = isLoginModalShowing => ({
  type: SHOW_LOGIN_MODAL,
  payload: {
    isLoginModalShowing,
  },
});

export const updateCurrentUser = currentUser => ({
  type: UPDATE_CURRENT_USER,
  payload: {
    currentUser,
  },
});

export const updateToken = token => ({
  type: UPDATE_TOKEN,
  payload: {
    token,
  },
});

export const login = values => async (dispatch, getState) => {
  try {
    const result = await http.post("auth/login/cms", values);

    if (result.user) {
      dispatch(updateCurrentUser(result.user));
    }

    if (result.token) {
      dispatch(updateToken(result.token));
      window.token = result.token;
    }

    return result;
  } catch (err) {
    throw err;
  }
};

export const register = values => async (dispatch, getState) => {
  try {
    const result = await http.post("auth/register", values);
    if (result.user) {
      dispatch(updateCurrentUser(result.user));
    }

    if (result.token) {
      dispatch(updateToken(result.token));
      window.token = result.token;
    }
    return result;
  } catch (err) {
    throw err;
  }
};

export const logout = () => ({
  type: LOGOUT,
});

export const updateProfile = payload => async (dispatch, getState) => {
  try {
    const result = await http.put("profile", payload);

    if (result.user) {
      dispatch(updateCurrentUser(result.user));
    }
    return result;
  } catch (err) {
    throw err;
  }
};

export const updatePassword = payload => async () => {
  try {
    const result = await http.put("profile/change-password", payload);

    return result;
  } catch (err) {
    throw err;
  }
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_LOGIN_MODAL:
      return {
        ...state,
        isLoginModalShowing: action.payload.isLoginModalShowing,
      };
    case UPDATE_CURRENT_USER:
      return {
        ...state,
        currentUser: action.payload.currentUser,
      };
    case UPDATE_TOKEN:
      return {
        ...state,
        token: action.payload.token,
      };
    case LOGOUT:
      return {
        ...state,
        token: undefined,
        currentUser: undefined,
      };
    default:
      return state;
  }
};
