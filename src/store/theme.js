const UPDATE_CORE_LAYOUT_CLASS = "theme/UPDATE_CORE_LAYOUT_CLASS";

export const initialState = {
  coreLayoutClass: "",
};

export const updateCoreLayoutClass = className => ({
  type: UPDATE_CORE_LAYOUT_CLASS,
  payload: {
    className,
  },
});

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_CORE_LAYOUT_CLASS:
      return {
        ...state,
        coreLayoutClass: action.payload.className,
      };
    default:
      return state;
  }
};
