import { createSelector } from "reselect";
import { each, map } from "lodash";

export const auth = state => state.auth;

export const getCurrentUser = createSelector(auth, state => state.currentUser);
export const isLoginModalShowing = createSelector(
  auth,
  state => state.isLoginModalShowing,
);

export const theme = state => state.theme;

export const getCoreLayoutClass = createSelector(
  theme,
  state => state.coreLayoutClass,
);

export const cart = state => state.cart;

export const getCartItems = createSelector(cart, state => map(state.items));
export const getCartTotal = createSelector(cart, state => {
  let total = 0;

  each(state.items, item => {
    total += item.quantity * item.price * item.currency.exchange_rate;
  });
  return total;
});
