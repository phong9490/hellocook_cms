import uuidv4 from "uuid/v4";
import http from "helpers/http";
import { omit } from "lodash";

import * as selectors from "./selectors";

const ADD_TO_CART = "cart/ADD_TO_CART";
const EMPTY_CART = "cart/EMPTY_CART";
const REMOVE_FROM_CART = "cart/REMOVE_FROM_CART";

export const initialState = {
  items: {},
};

export const addToCart = item => ({
  type: ADD_TO_CART,
  payload: {
    item,
  },
});

export const emptyCart = item => ({
  type: EMPTY_CART,
});

export const removeFromCart = item => ({
  type: REMOVE_FROM_CART,
  payload: {
    item,
  },
});

export const placeOrder = values => async (dispatch, getState) => {
  const state = getState();
  const items = selectors.getCartItems(state);
  try {
    const result = await http.post("customer/invoices", {
      ...values,
      order_items: items,
    });

    return result;
  } catch (err) {
    throw err;
  }
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case EMPTY_CART:
      return {
        ...state,
        items: initialState.items,
      };
    case ADD_TO_CART: {
      const item = {
        id: uuidv4(),
        ...action.payload.item,
      };
      return {
        ...state,
        items: {
          ...state.items,
          [item.id]: item,
        },
      };
    }
    case REMOVE_FROM_CART:
      return {
        ...state,
        items: omit(state.items, action.payload.item.id),
      };
    default:
      return state;
  }
};
