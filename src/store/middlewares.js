import { SubmissionError } from "redux-form";
import { toast } from "react-toastify";
import { isString, mapValues, map, isArray } from "lodash";

const handleError = store => err => {
  // handle object error
  if (err.errors) {
    // check state if a redux-form is submitting state
    const formState = store.getState().form;
    for (const key in formState) {
      const form = formState[key];
      if (form.submitting) {
        const submissionError = mapValues(
          err.errors,
          error => (isArray(error) ? error.join(".") : error),
        );
        throw new SubmissionError(submissionError);
      }
    }
    // convert to string message
    const stringMessage = map(err.errors, error => error).join(",");
    return toast.error(stringMessage);
  }
  // handle string error
  if (err.error && isString(err.error)) {
    return toast.error(err.error);
  }
};

const applyErrorHandler = (fn, store) => (...args) => {
  const promise = fn(...args);
  if (promise instanceof Promise) {
    return promise.catch(handleError(store));
  }
  return promise;
};

export const handleGlobalError = store => next => action => {
  if (typeof action === "function") {
    return next(applyErrorHandler(action, store));
  }
  return next(action);
};
