import { connect } from "react-redux";

import * as selectors from "store/selectors";

import CoreLayout from "./CoreLayout";

const mapStateToProps = state => ({
  className: selectors.getCoreLayoutClass(state),
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(CoreLayout);
