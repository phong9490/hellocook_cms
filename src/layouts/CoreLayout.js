import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";

const CoreLayout = ({ children, className }) => (
  <div className={cx("core-layout", className)}>{children}</div>
);

CoreLayout.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};

export default CoreLayout;
